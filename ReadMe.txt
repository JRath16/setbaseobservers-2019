%---- Instructions for Running -----%
% Author- J. J. Rath


*Please note that the following three packages are required to run and analyse any of the presented results.
%------------Prequisites----------%
1. CORA - Avialiable at: https://tumcps.github.io/CORA/
2. YALMIP - Avialiable at: https://yalmip.github.io/
3. MOSEK Solver - Availiable at: https://www.mosek.com/
%----------


%-- For a single test---
Syntax:
      [GSE] = SingleTest_VSE(ModelDim,Estimator,RedTech,ZOrder,ts,T,Disturbance)

Example: 
      [GSE] = SingleTest_VSE('2',SegMin,'combastel','50',0.01,4,'random')   

%--- For a collectective test of all estimators---

Run:
      VSEPerformance
%-----

* For the single test scenario the appropiate optimziation algorithm must be selcted in case of offline gain based observer. All optimizers are given 
in the file: OffLineGainCompute.m

** For collective run of all estimators, in the VSE Performance file the tests are conducted for:
    - Tweleve zonotopic observers and five ellipsoidal observers (All observers can be found in the Estimators folder)
    - Each estimator is evaluated for vehcile model of two, four, and six dimenions
    - For all zonotopic observers - order is varied between 25 to 150
    - For all zonotopic observers - four reduction techniques are considered.
*** The vehcile model and parameters are given in files VehicleParametersDefault.m and DynamicVehicleRun.m respectively. 
