function EstSet = ESO_A_Test_SMA(Estimator,PpD,xh0,options)
% Ref Paper -1: Set Membership State Estimation with Optimal Bounding
% Ellipsoids, S. Gollamudi, S. Nagraj, S. Kapoor and Y. F. Huang, 1996




ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
EstSet.EXh = xh0.EXin;
EstSet.EXp = xh0.EXin;
EstSet.Xhp(:,1) = center(xh0.EXin);
EstSet.Xpp(:,1) = center(xh0.EXin);
EstSet.XhH{1} =eye(size(PpD.E,1));
EstSet.XpH{1} =eye(size(PpD.E,1));
EstSet.ELamOp{1} = 0;
%------------------------------------------
EstSet.ESig{1} = 1; % REF1-The parameter 'sigma' from Theorem 2
EstSet.alpha = 0.00009; % REF1-Parameter 'alpha' from Theorem 2
EstSet.Egam = max(max(PpD.F'*PpD.F)); % REF1-Parameter 'gamma' from Theorem 2
EstSet.so1 = sqrt(trace(eye(size(PpD.A,1)))); % REF2- sqrt(Trace(Wk))
%-----------------------------------
for k = 1:length(tspan)-2
    EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted ellipsoid at k+1 instant % Ref 2- Eq 21
    %-- The predicted elliposid is computed as discussed in Paper 2- i.e. using minimal trace criterion  (Ref2- SECTION 3)
    EstSet.Ep{k+1} = EstSet.so1/(sqrt(trace(EstSet.ESig{k}*EstSet.ESig{k}*PpD.A*EstSet.XhH{k}*PpD.A')) + EstSet.so1 ); % Ref 2- Eq 48
    EstSet.XpH{k+1} = inv(1-EstSet.Ep{k+1})*PpD.A*EstSet.XhH{k}*PpD.A' + inv(EstSet.ESig{k}*EstSet.ESig{k}*EstSet.Ep{k+1})*PpD.E; % Ref 2- Eq.21
    EstSet.ESigP{k+1} = real(sqrt(EstSet.ESig{k})); % Ref 2- Eq 22
    EstSet.EXp(:,k+1) = ellipsoid(inv(EstSet.ESigP{k+1}*EstSet.ESigP{k+1}*(EstSet.XpH{k+1})),EstSet.Xpp(:,k+1)); % The predicted ellipsoid
    %---- Update Step: Paper 2
    EstSet.EDel{k+1} =  PpD.Yp(:,k+1)-PpD.C*EstSet.Xpp(:,k+1); % PAPER 1: Theorem 1
    EstSet.EG{k+1} = PpD.C*EstSet.XpH{k+1}*PpD.C'; % Paper 1- Theorem 1
    EstSet.Eg{k+1} = svds(EstSet.EG{k+1},1);  % Compute the maximum singular values of EG, Paper 1- Below eq (17)
    EstSet.Ebeta{k+1} = (1- EstSet.ESigP{k+1}*EstSet.ESigP{k+1})/(EstSet.EDel{k+1}'*EstSet.EDel{k+1}); % Paper 1- Eq. 18
    %--- Gain Compute
    EstSet.ELamOp{k+1} = Estimator.ComputeGainESO_MIMO(EstSet.ESigP{k+1},EstSet.EDel{k+1},EstSet.Eg{k+1},EstSet.Ebeta{k+1},EstSet.Egam,EstSet.alpha); % Theorem 2
    %--- Time Update Step
    EstSet.EQ{k+1}  = (1-EstSet.ELamOp{k+1})*eye(size(PpD.C,1)) + EstSet.ELamOp{k+1}*EstSet.EG{k+1}; % Paper 1- Theorem 1
    EstSet.EP1{k+1} = (1-EstSet.ELamOp{k+1})*inv(EstSet.XpH{k+1}) + EstSet.ELamOp{k+1}*PpD.C'*PpD.C; % Paper 1- Theorem 1
    EstSet.ESig{k+1} = sqrt((1-EstSet.ELamOp{k+1})*EstSet.ESigP{k+1}*EstSet.ESigP{k+1} + ...
        EstSet.ELamOp{k+1}*EstSet.Egam*EstSet.Egam...
        -EstSet.ELamOp{k+1}*(1-EstSet.ELamOp{k+1})*EstSet.EDel{k+1}'*inv(EstSet.EQ{k+1})*EstSet.EDel{k+1} ); % Paper 1- Theorem 1
    EstSet.Xhp(:,k+1) = EstSet.Xpp(:,k+1) + EstSet.ELamOp{k+1}*inv(EstSet.EP1{k+1})*PpD.C'*EstSet.EDel{k+1};% Paper 1- Theorem 1
    EstSet.XhH{k+1} = (EstSet.EP1{k+1});% Paper 1- Theorem 1
    EstSet.EXh(:,k+1) = ellipsoid(inv (EstSet.ESig{k+1}*EstSet.ESig{k+1}*inv(EstSet.XhH{k+1})),EstSet.Xhp(:,k+1));% Paper 1- Theorem 1  
end
end