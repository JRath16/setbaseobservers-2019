
function VehParam = VehicleParametersDefault()


mu=1; Vx = 15;
lr = 1.508; lf = 0.883; m =1.225e3 ; g = 9.81;  Iz = 1.538e3;
Cf = 45000; Cr = 57000;  % These paramters have been taken from CommonRoad vehicle paramters considering friction = 1,  However if both values are same, then observability is lost;
Bs = 0.5; Js = 0.05; etat = 0.010; Ks = 0; Rs = 16;
%------Vehicle Paramters-----------%
Fzf = m*g*lr/(lf+lr); % front tire vertical force
Fzr = m*g*lf/(lf+lr); % rear tire vertical force
Csf = Cf/(mu*Fzf);Csr =Cr/(mu*Fzr);
%------- System Dynamics-----------%
C1 = mu/(Vx*(lr+lf)); C2 = mu*m/(Iz*(lr+lf)); VehParam.a11 = -C1*g*(Csr*lf-Csf*lr);
VehParam.a12 = C1*(g*lf*lr/Vx)*(Csr-Csf) - 1; VehParam.a21 = C2*g*lr*lf*(Csr-Csf); VehParam.a22 = -C2*(g/Vx)*(lf*lf*lr*Csf + lr*lr*lf*Csr);
VehParam.b11 = C1*g*lr*Csf; VehParam.b12 = C2*g*lf*lr*Csf; 
VehParam.a61 = etat*mu*Csf*Fzf/Js;
VehParam.a62 = etat*mu*Csf*Fzf*lf/(Js*Vx);
VehParam.a65 = -etat*mu*Csf*Fzf/Js;
VehParam.a66  = -Rs*Bs/Js;
VehParam.b16 = Rs/Js;
VehParam.Vx = Vx;
VehParam.IP = 2;

end