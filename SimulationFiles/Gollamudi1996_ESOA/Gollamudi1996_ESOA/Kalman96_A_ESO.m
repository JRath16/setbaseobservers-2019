function Estimator = Kalman96_A_ESO()
% Ref Paper 1: Set Membership State Estimation with Optimal Bounding
% Ellipsoids, S. Gollamudi, S. Nagraj, S. Kapoor and Y. F. Huang, 1996


Estimator.Name = 'Gollamudi96_ESO';%ESO_A
Estimator.Type = 'SMA';
Estimator.Design = 'mimo';
Estimator.SetRep = 'ellipsoidal';
Estimator.ComputeGainESO_MIMO =@(ESig,EDel,Eg,Ebeta,Egam,alpha)ComputeGainESO_MIMO(ESig,EDel,Eg,Ebeta,Egam,alpha);
end

function OGain = ComputeGainESO_MIMO(ESig,EDel,Eg,Ebeta,Egam,alpha)
%-----------------------
%-- Theorem 2 from Paper 1
% ------  Condition for not updating the gain
if ((ESig*ESig) + (EDel'*EDel)) <= Egam*Egam   
    OGain = 0;
else
    %--- Conditions for updating the gain
    if EDel'*EDel == 0  % Case 1
        Enu = alpha;
    elseif Eg == 1      % Case 2
        Enu = (1-Ebeta)/2;
    elseif (1+Ebeta*(Eg-1))>0 % Case 3  (w = (1+Ebeta*(Eg-1)) in the paper
        Enu = (1/(1-Eg))*(1 - sqrt(Eg/(1+Ebeta*(Eg-1))));
    elseif (1+Ebeta*(Eg-1)) <=0  % Case 4
        Enu = alpha;
    end
    %-------------------
    OGain = min(alpha,Enu); % The optimal value of the updated gain
end
%-----------------------
end
