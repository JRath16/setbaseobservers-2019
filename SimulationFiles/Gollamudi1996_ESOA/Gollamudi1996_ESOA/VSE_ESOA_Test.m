function [GSEstimation] = VSE_ESOA_Test(PpD,Estimator,options)
Ttotal = 0:options.stepSize:options.T-options.stepSize;
   %------ Sensor Noise%--------
    for i = 1:size(PpD.C,1)
        PpD.vk(:,i) = 0 + 0.33*randn(length(Ttotal),1);
        PpD.vk(:,i) = SaturateNoise(PpD.vk(:,i),-1,1);
    end
    %--------------
PpD.Yp = PpD.Y + PpD.F*PpD.vk';
%--------------
Dim = size(PpD.A,1);
    xh0.EXin = ellipsoid(eye(size(PpD.A,1)),zeros(size(PpD.A,1),1)); % Initial State bounded in unity box
    xh0.EW =  ellipsoid(eye(size(PpD.E,1)),zeros(size(PpD.E,1),1)); xh0.EV =  ellipsoid(eye(size(PpD.C,1)),zeros(size(PpD.C,1),1));
EstN = Estimator.Name;
GSEstimation = Sim_ESOA_Test(Estimator,PpD,xh0,options);

end