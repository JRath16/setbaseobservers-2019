%% Simulate Plant and Obserever- Set RepresenatatioBase
function [GSEstimation] = Sim_ESOA_Test(Estimator,PpD,xh0,options)

%% Inputs:


%% Initilize system
ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%% SIMULATE OBSERVER
tic;   
EstSet =ESO_A_Test_SMA(Estimator,PpD,xh0,options);



EstSet.Intertt = 0; 
TComp = toc-EstSet.Intertt;
% end%% Performance Computation
GSEstimation.Name = Estimator.Name;GSEstimation.Type = Estimator.Type;
GSEstimation.SetRep = Estimator.SetRep;
GSEstimation.EstStates = EstSet;
GSEstimation.TPlant = PpD;
GSEstimation.InitialObserver = xh0;
GSEstimation.SampleStepSize = options.stepSize ;
% %--------------
PerfSet = PC_ESOA_Test(PpD,EstSet,options);
GSEstimation.Performance = PerfSet ;
GSEstimation.CenterError = PerfSet.ERMS;
GSEstimation.Xhmax = PerfSet.Xhmax;
GSEstimation.Xhmin = PerfSet.Xhmin;
GSEstimation.IRadius = PerfSet.IRadius;
% %-------------
GSEstimation.TimePIter = TComp/length(tspan);
end