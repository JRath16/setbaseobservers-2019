function PerfSet = PC_ESOA_Test(PpD,EstSet,options)

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
        for i = 1:length(tspan)-1
            [PerfSet.BXh(:,i), PerfSet.IXh(:,i),EstSet.ZXh(:,i)]  = Interval_EllipsiodJR((EstSet.EXh(i)));
            boundse = EllipsoidProj(EstSet.EXh(:,i));
            PerfSet.Xhmax(:,i) = boundse.Up;
            PerfSet.Xhmin(:,i) = boundse.Low;
%             PerfSet.VolZXh(i) = volumeApprox(EstSet.ZXh(:,i));PerfSet.VolBXh(i) = volumeApprox(PerfSet.BXh(:,i));
            PerfSet.Xe(:,i) =  norm(EstSet.Xhp(:,i)-PpD.X(:,i));
            PerfSet.IRad(:,i) = 0.5*(PerfSet.Xhmax(:,i)-PerfSet.Xhmin(:,i));
        end
PerfSet.ERMS = rms(PerfSet.Xe);
% PerfSet.ERMS1 = rms(PerfSet.Xe);

for i = 1:size(PpD.A,1)
    PerfSet.IRadius(i) = rms(PerfSet.IRad(i,:));
end
end