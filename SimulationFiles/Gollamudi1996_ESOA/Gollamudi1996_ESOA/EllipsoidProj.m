function Bounds = EllipsoidProj(E,rho)

if nargin <=1
    rho_p = 1;
else
    rho_p = rho;
end
c = E.q;
P = E.Q;



%-------------
M1 = P/rho_p;
M1 = inv(M1);
M1= sqrt(M1);
M = M1(sub2ind(size(M1),1:size(M1,1),1:size(M1,2)));


Bounds.Up = c +M';
Bounds.Low = c - M';
end