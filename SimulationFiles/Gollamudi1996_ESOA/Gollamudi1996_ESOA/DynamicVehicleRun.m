function [PpD,options] = DynamicVehicleRun(ModelDim,ts,T)

VehParam = VehicleParametersDefault();
options.stepSize = ts;
options.T = T;

Ttotal = 0:options.stepSize:options.T-options.stepSize;
%--------------------------
if strcmp(ModelDim,'2')
    Dim =2; Mes = 1;
elseif strcmp(ModelDim,'4')
    Dim =4; Mes =3;
elseif strcmp(ModelDim,'6')
    Dim =6; Mes =4;
end
%----------------------
for i = 1:(Mes)
    Vk(i)= 0.0 + (0.1 - 0.0)*sum(rand(1,6),2)/6;
end
for i = 1:(Dim)
    Wk(i)= 0.005 + (0.05 - 0.005)*sum(rand(1,6),2)/6;
end

%------------------
Vk = 0.1*ones(1,6);
Wk = [0.005,0.055,0.005,0.04,0.02,0.2];
%--------------------------
if Dim ==2 % 2 states model
    A1 = [VehParam.a11,VehParam.a12;VehParam.a21,VehParam.a22];
    B1 = [VehParam.b11;VehParam.b12];
    C1 = [0 VehParam.Vx]; % single measurement
elseif Dim ==4 % 4 states model
    A1 = [VehParam.a11,VehParam.a12,0,0;VehParam.a21,VehParam.a22,0,0; 0,1,0,0; VehParam.Vx,VehParam.Vx,0,0];
    B1 = [VehParam.b11;VehParam.b12;0;0];
    C1 = [0 VehParam.Vx 0 0; 0 0 1 0; 0 0 0 1]; % 3 measurements
elseif Dim==6
    A1 =  [VehParam.a11,VehParam.a12,0,0, VehParam.b11,0; VehParam.a21,VehParam.a22,0,0, VehParam.b12,0; 0,1,0,0,0,0; VehParam.Vx,VehParam.Vx,0,0,0,0; 0,0,0,0,0,1;VehParam.a61,VehParam.a62,0,0,VehParam.a65,VehParam.a66];
    B1 = [0;0;0;0;0;VehParam.b16];
    C1 = [0 VehParam.Vx 0 0 0 0; 0 0 1 0 0 0; 0 0 0 1 0 0; 0 0 0 0 1 0]; % 4 measurements
end
%-----------------------------------------------
%----- A sample Input Signal
NCurv = 100; % The max samples for which the steer input is not zero
%-----
if Dim == 6
    Nmag1 = VehParam.IP; % The steer magnitude - Seg 1
else
    Nmag1 = 0.1*VehParam.IP;
end
%----------
InpZero = length(0:options.stepSize:options.T)-2*NCurv-100;  % For two segments
InpSig = [zeros(50,1);Nmag1*ones(NCurv,1);zeros(50,1);-Nmag1*ones(NCurv,1); zeros((InpZero),1)];
%-----------
PpD.F = diag(Vk(1:Mes));
PpD.E = diag(Wk(1:Dim));
PlantCT = ss(A1,B1,C1,[]); % Continious Time Plant
PlantDT = c2d(PlantCT,options.stepSize,'impulse'); % Discretized Plant
PlantDT = ss(PlantDT.A,[PlantDT.B PpD.E],PlantDT.C,0,options.stepSize); % Integrated Discretized New Plant with
PpD.A = PlantDT.A; PpD.B = PlantDT.B(:,1:end-size(PpD.E,1));
PpD.E = PlantDT.B(:,size(PpD.B,2)+1:end);PpD.C = PlantDT.C;
%--------- Sensor Noise Effects----------------------------------
 %------Process Noise%-------
    for i = 1:size(PpD.A,1)
        PpD.wk(:,i) = 0 + 0.33*randn(length(Ttotal),1);
        PpD.wk(:,i) = SaturateNoise(PpD.wk(:,i),-1,1);
    end

%-------------------------
%----- Simulate the Plant- with input as process noise----------------%
[Y,t,X] = lsim(PlantDT,[InpSig(1:length(Ttotal),1) PpD.wk],Ttotal); % Simulate the Plant
PpD.Y = Y';PpD.X =X';PpD.U =  InpSig';
end