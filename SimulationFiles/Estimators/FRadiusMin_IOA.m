function Estimator = FRadiusMin_IOA()
%Refernce- R1: Christophe Combastel. Zonotopes and kalman observers:
%        Gain optimality under distinct uncertainty paradigms and
%        robust convergence. Automatica, 55:265-273, 2015.


Estimator.Name = 'FRadIOA';
Estimator.Type = 'IOA';
Estimator.Design = 'state';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainMIMO =@(PpD,XhH,OffGain)ComputeGainMIMO(PpD,XhH,OffGain);
end

function OGain = ComputeGainMIMO(PpD,XhH,OffGain) % Theorem 5, of R1
Pr = XhH*XhH';
Qw = PpD.F*PpD.F'; L = Pr*PpD.C'; S = PpD.C*L + Qw; 
Kst = L*inv(S); 
Gst = PpD.A*Kst; 
OGain = Gst;  
end
