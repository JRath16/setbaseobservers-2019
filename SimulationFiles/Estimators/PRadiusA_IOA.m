function Estimator = PRadiusA_IOA()
Estimator.Name = 'PRadAIOA';
Estimator.Type = 'IOA';
Estimator.Design = 'state';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainMIMO =@(PpD,XhH,OffGain)ComputeGainMIMO(PpD,XhH,OffGain);
end

function OGain = ComputeGainMIMO(PpD,XhH,OffGain)
 OGain =  OffGain.IOA_PRadA;
end