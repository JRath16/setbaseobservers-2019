function Estimator = PRadiusE()



Estimator.Name = 'PRadE';
Estimator.Type = 'SMA';
Estimator.Design = 'mimo';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainMIMO =@(PpD,XhH,OffGain)ComputeGainMIMO(PpD,XhH,OffGain);
end

function OGain = ComputeGainMIMO(PpD,Xh,OffGain)
 OGain =  OffGain.SMA_PRadE;
end