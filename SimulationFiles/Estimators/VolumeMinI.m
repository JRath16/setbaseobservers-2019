function Estimator =  VolumeMinI()



Estimator.Name = 'VolMinI';
Estimator.Type = 'SMA';
Estimator.Design = 'siso';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainSISO = @(PpD,C,sig,XhH,Xhc,Yp,j,OffGain)ComputeGainSISO(PpD,C,sig,XhH,Xhc,Yp,j,OffGain);
end

function OGain = ComputeGainSISO(PpD,C,sig,XhH,Xhc,Yp,j,OffGain)
fun_m = @(Lambda)volume(zonotope([Xhc  + Lambda*(Yp-(C*Xhc)), [(eye(size(PpD.A,1))-Lambda*C)*XhH,(sig)*Lambda]]));
[Lambda]  =  fminsearch(fun_m,zeros(size(PpD.A,1),size(PpD.C,1))); 
 OGain = Lambda;    
end