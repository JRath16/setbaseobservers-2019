function Estimator = Kalman16_C_ESO()

Estimator.Name = 'ESO_C';
Estimator.Type = 'SMA';
Estimator.Design = 'mimo';
Estimator.SetRep = 'ellipsoidal';
Estimator.ComputeGainESO_MIMO =@(ESig,EDel,Eg,Ebeta,Egam,alpha)ComputeGainESO_MIMO(ESig,EDel,Eg,Ebeta,Egam,alpha);
end

function OGain = ComputeGainESO_MIMO(ESig,EDel,Eg,Ebeta,Egam,alpha)
if ((ESig*ESig) + (EDel'*EDel)) <= 1
    OGain = 0;
end
if ((ESig*ESig) + (EDel'*EDel)) > 1
    if Eg == 1
        OGain = (1-Ebeta)/2;
    elseif  Eg ~= 1
        OGain = (1/(1-Eg))*(1 - sqrt(Eg/(1+Ebeta*(Eg-1))));
    end
end
end
