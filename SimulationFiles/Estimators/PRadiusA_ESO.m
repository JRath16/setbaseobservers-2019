function Estimator = PRadiusA_ESO()



Estimator.Name = 'PRadAESO';
Estimator.Type = 'IOA';
Estimator.Design = 'state';
Estimator.SetRep = 'ellipsoidal';
Estimator.ComputeGainMIMO =@(PpD,XhH,OffGain)ComputeGainMIMO(PpD,XhH,OffGain);
end

function OGain = ComputeGainMIMO(PpD,Xh,OffGain)
 OGain =  OffGain.EllipIOA_PRadA;
end