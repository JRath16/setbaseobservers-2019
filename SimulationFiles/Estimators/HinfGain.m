function Estimator = HinfGain()
Estimator.Name = 'HinfG';
Estimator.Type = 'IOA';
Estimator.Design = 'error';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainMIMO =@(PpD,XhH,OffGain)ComputeGainMIMO(PpD,XhH,OffGain);
end

function OGain = ComputeGainMIMO(PpD,XhH,OffGain)
 OGain =  OffGain.IOA_HInfG;
end