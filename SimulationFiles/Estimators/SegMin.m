function Estimator =  SegMin()



Estimator.Name = 'SegMin';
Estimator.Type = 'SMA';
Estimator.Design = 'siso';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainSISO = @(PpD,C,sig,XhH,Xhc,Yp,j,OffGain)ComputeGainSISO(PpD,C,sig,XhH,Xhc,Yp,j,OffGain);
end

function OGain = ComputeGainSISO(PpD,C,sig,XhH,Xhc,Yp,j,OffGain)
 X1 = XhH*XhH'; X2 = X1*C';X3 = C*X1*C' + sig*sig';
 OGain = X2/X3;
end