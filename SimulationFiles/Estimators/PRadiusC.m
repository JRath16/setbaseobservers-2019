function Estimator = PRadiusC()



Estimator.Name = 'PRadC';
Estimator.Type = 'SMA';
Estimator.Design = 'mimo';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainMIMO =@(PpD,XhH,OffGain)ComputeGainMIMO(PpD,XhH,OffGain);
end

function OGain = ComputeGainMIMO(PpD,Xh,OffGain)
 OGain =  OffGain.SMA_PRadC;
end