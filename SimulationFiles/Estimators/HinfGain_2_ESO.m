function Estimator = HinfGain_2_ESO()



Estimator.Name = 'HinfG_ESO2';
Estimator.Type = 'IOA';
Estimator.Design = 'error';
Estimator.SetRep = 'ellipsoidal';
Estimator.ComputeGainMIMO =@(PpD,XhH,OffGain)ComputeGainMIMO(PpD,XhH,OffGain);
end

function OGain = ComputeGainMIMO(PpD,Xh,OffGain)
 OGain =  OffGain.Ellip_Hinf_2;
end