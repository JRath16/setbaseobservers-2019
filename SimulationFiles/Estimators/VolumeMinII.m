function Estimator  = VolumeMinII()

% Inputs: Zonotope(Z), the row vector of output strip (c), measurement (d) and noise bound (sig)
% Ref paper: Alamo et.al, Bounded error identification of systems with time-varying
% parameters, TAC 2006
% Author:
% Date: 17.09.2019
Estimator.Name = 'VolMinII';
Estimator.Type = 'SMA';
Estimator.Design = 'siso';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainSISO = @(PpD,C,sig,XhH,Xhc,Yp,j,OffGain)ComputeGainSISO(PpD,C,sig,XhH,Xhc,Yp,j,OffGain);
end

function OGain = ComputeGainSISO(PpD,C,sig,XhH,Xhc,Yp,j,OffGain)
d= Yp;p = Xhc; H = XhH;c =C;
[dim,nrGen]=size(H);
% No of computations = nrGen+1
for j = 0:nrGen
    if j == 0
        v(:,j+1) = p;
        T{j+1} = H;
    elseif  c*H(:,j)~=zeros(dim,1)
        for i =1:nrGen
            if i~=j
                T1{i,j} = H(:,i)  - (c*H(:,i))/(c*H(:,j))*H(:,j);
            elseif i == j
                T1{i,j} = ((sig/(c*H(:,j)))).*H(:,j);
            end
        end
        T{j+1} = [T1{:,1:end}];
        v(:,j+1) =  p + ((d-c*p)./(c*H(:,j)))*H(:,j);
        clear T1
    elseif c*H(:,j)==zeros(dim,1)
        v(:,j+1) = p;
        T{j+1} = H;
    end
    Z(j+1) = zonotope([v(:,j+1),T{j+1}]);
    %---------------------------------------
    G=Z(j+1).Z(:,2:end);
    volApprxZ(j+1)  = ((det(G(:,1:end)*(G(:,1:end))')));
    %-------------------
    clear G
end
%-- The minimal volume zonotope
[~,z] = min(volApprxZ);

OGain = Z(z);

end
