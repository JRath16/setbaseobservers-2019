function Estimator = Kalman96_B_ESO()



Estimator.Name = 'ESO_B';
Estimator.Type = 'SMA';
Estimator.Design = 'mimo';
Estimator.SetRep = 'ellipsoidal';
Estimator.ComputeGainESO_MIMO =@(ESig,EDel,Eg,Ebeta,Egam,alpha)ComputeGainESO_MIMO(ESig,EDel,Eg,Ebeta,Egam,alpha);
end

function OGain = ComputeGainESO_MIMO(ESig,EDel,Eg,Ebeta,Egam,alpha)
%-----------------------
if ((ESig*ESig) + (EDel'*EDel)) <= Egam*Egam
    OGain = 0;
else
    if EDel'*EDel == 0
        Enu = alpha;
    elseif Eg == 1
        Enu = (1-Ebeta)/2;
    elseif (1+Ebeta*(Eg-1))>0
        Enu = (1/(1-Eg))*(1 - sqrt(Eg/(1+Ebeta*(Eg-1))));
    elseif (1+Ebeta*(Eg-1)) <=0
        Enu = alpha;
    end
    OGain = min(alpha,Enu);
end
%-----------------------
end
