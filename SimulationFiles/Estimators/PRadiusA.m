function Estimator = PRadiusA()



Estimator.Name = 'PRadA';
Estimator.Type = 'SMA';
Estimator.Design = 'siso';
Estimator.SetRep = 'zonotopic';
Estimator.ComputeGainSISO = @(PpD,C,sig,XhH,Xhc,Yp,j,OffGain)ComputeGainSISO(PpD,C,sig,XhH,Xhc,Yp,j,OffGain);
end

function OGain = ComputeGainSISO(PpD,C,sig,XhH,Xhc,Yp,j,OffGain);
 OGain =  OffGain.SMA_PRadA(:,j);
end