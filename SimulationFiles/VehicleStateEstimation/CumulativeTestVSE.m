function [GSEstimation] = CumulativeTestVSE(PpD,Estimator,RedTech,ZOrder,OffGain,options,Disturbance)

israndom = strcmp(Disturbance,'random');
issquare = strcmp(Disturbance,'square');

Ttotal = 0:options.stepSize:options.T-options.stepSize;

if israndom
    %------ Sensor Noise%--------
    for i = 1:size(PpD.C,1)
        PpD.vk(:,i) = 0 + 0.33*randn(length(Ttotal),1);
        PpD.vk(:,i) = SaturateNoise(PpD.vk(:,i),-1,1);
    end
    %--------------
elseif issquare
    %------ Sensor Noise%--------
    for i = 1:size(PpD.C,1)
        PpD.vk(:,i) = square(Ttotal,30)';
    end
    %--------------
end
PpD.Yp = PpD.Y + PpD.F*PpD.vk';
%--------------
Dim = size(PpD.A,1);
options.RedTech = RedTech; % Zonotopic order reduction technique
options.ZOrder = round((ZOrder)/Dim);
%-------------------------------------------------------------------
isZSE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'zonotopic'));   % Check if zonotopic set representation
isESE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'ellipsoidal'));   % Check if zonotopic set representation
%---------------------------------------
if isESE
    xh0.EXin = ellipsoid(eye(size(PpD.A,1)),zeros(size(PpD.A,1),1)); % Initial State bounded in unity box
    xh0.EW =  ellipsoid(eye(size(PpD.E,1)),zeros(size(PpD.E,1),1)); xh0.EV =  ellipsoid(eye(size(PpD.C,1)),zeros(size(PpD.C,1),1));
elseif isZSE
    xh0.ZXin = zonotope([zeros(size(PpD.A,1),1),eye(size(PpD.A,1))]); % Initial State bounded in unity box
    xh0.ZW =  zonotope([zeros(size(PpD.E,1),1),eye(size(PpD.E,1))]); xh0.ZV = zonotope([zeros(size(PpD.C,1),1),eye(size(PpD.C,1))]);
end

EstN = Estimator.Name;
try
    GSEstimation = EstSimulate(Estimator,PpD,xh0,options,OffGain);
catch
    Xdis = ['Cannot Compute for ', EstN, ' plant dim ', Dim,  ' and reduction tech ' ,RedTech ]; disp(Xdis);
    GSEstimation = [];
end

end