%% A function to run an Estimation technique
function [GSEstimation] = SingleTest_VSE(ModelDim,Estimator,RedTech,ZOrder,ts,T,Disturbance)

% savepath='.\VehicleStateEstimation\Results'; 

PpD = DynamicVehicleRun(ModelDim,ts,T);
%---- Observability/Detctability Check%--------
Ch = rank(PpD.A)-rank(obsv(PpD.A,PpD.C));
%--------------
if(Ch ==0) % If observable
[OffGain.IOA_HInfG2_State,OffGain.IOA_W_HInfG2_State,OffGain.IOA_T_HInfG2_State,OffGain.ObsIndHInfG2_State,OffGain.Gamma_HInfG2_State] =   HInfinity2_ZIOA(PpD); % H inifinity
% [OffGain.IOA_HInfG,OffGain.IOA_W_HInfG,OffGain.IOA_T_HInfG,OffGain.ObsIndHinfG,OffGain.Gamma_Hinf] =   HInfinity_IOA(PpD); % H inifinity
else
disp('System Not Observable')
end
%--------------
Dim = size(PpD.A,1);
options.RedTech = RedTech; % Zonotopic order reduction technique
options.ZOrder =round(str2double(ZOrder)/Dim);
options.stepSize = ts;
options.T = T;
Ttotal = 0:options.stepSize:options.T-options.stepSize;

%-------------------------------------------------------------------
isZSE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'zonotopic'));   % Check if zonotopic set representation
isESE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'ellipsoidal'));   % Check if zonotopic set representation
%---------------------------------------
if isESE
xh0.EXin = ellipsoid(eye(size(PpD.A,1)),zeros(size(PpD.A,1),1)); % Initial State bounded in unity box
xh0.EW =  ellipsoid(eye(size(PpD.E,1)),zeros(size(PpD.E,1),1)); xh0.EV =  ellipsoid(eye(size(PpD.C,1)),zeros(size(PpD.C,1),1));     
elseif isZSE
xh0.ZXin = zonotope([zeros(size(PpD.A,1),1),eye(size(PpD.A,1))]); % Initial State bounded in unity box
xh0.ZW =  zonotope([zeros(size(PpD.E,1),1),eye(size(PpD.E,1))]); xh0.ZV = zonotope([zeros(size(PpD.C,1),1),eye(size(PpD.C,1))]);
end

israndom = strcmp(Disturbance,'random');
issquare = strcmp(Disturbance,'square');

if israndom
    %------ Sensor Noise%--------
    for i = 1:size(PpD.C,1)
        PpD.vk(:,i) = 0 + 0.33*randn(length(Ttotal),1);
        PpD.vk(:,i) = SaturateNoise(PpD.vk(:,i),-1,1);
    end
    %--------------
elseif issquare
    %------ Sensor Noise%--------
    for i = 1:size(PpD.C,1)
        PpD.vk(:,i) = square(Ttotal,30)';
    end
    %--------------
end
PpD.Yp = PpD.Y + PpD.F*PpD.vk';
%------
try
GSEstimation = EstSimulate(Estimator,PpD,xh0,options,OffGain);
catch
   disp('Error in computing the estimated set')
  GSEstimation = []; 
end
% %-----------------SAVE Results
% dim = num2str(Dim); EstN = Estimator.Name; EstT = Estimator.Type;
% save([savepath '\' dim 'States_' EstN '_' EstT '_' RedTech '_' date '.mat'], 'dim' ,'EstN','EstT','RedTech','GSEstimation');
end