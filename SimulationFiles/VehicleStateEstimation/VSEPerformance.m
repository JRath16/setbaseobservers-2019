function VSEPerformance()
savepath='.\VehicleStateEstimation\Results';
close all
ZnOrder= [25,50,75,100,125,150];
ts = 0.01;
T = 4;
%-------------------------
ModelDim = {'2','4','6'};
Estimator = {
    VolumeMinI
    VolumeMinII
    SegMin
    FRadiusMin
    PRadiusA
    PRadiusB
    PRadiusC
    PRadiusD
    PRadiusDII
    FRadiusMin_IOA
    PRadiusA_IOA
    NominalGain
    HinfGain
    PRadiusA_ESO
    Kalman16_C_ESO
    Kalman96_A_ESO
    Kalman96_B_ESO
    HinfGain_1_ESO
    HinfGain_2_ESO
    };
RedTech = {
    'combastel'
    'Wtcombastel'
    'girard'
    'pca'
    };
Disturbance = {'random'};
%-------------------------------------------%
for ja = 1:length(ModelDim)
    disp('Computing Gains ...')
    [PpD, options] = DynamicVehicleRun(ModelDim{ja},ts,T);
    OffGain = OffLineGainCompute(PpD);
    clc;
    disp('Starting Estimation...')
    Dim = ModelDim{ja};
    Dist = Disturbance{1};
    for soo = 1:length(ZnOrder)
        ZOrder = ZnOrder(soo);
        for k = 1:length(Estimator)
            %--------- Special Case Volume Min -I Approach
               if Dim == '2'
                    EstN1 = Estimator{k}.Name;
                    if strcmp(EstN1,'VolMinI')
                        for s = 1:length(RedTech)
                            GE{k,s} = CumulativeTestVSE(PpD,Estimator{k},RedTech{s},ZOrder,OffGain,options,Disturbance{1});
                        end
                        Xdis = ['The completed Estimator is ', EstN1, ' for plant dim ' Dim ]; disp(Xdis);
                    end
                end
            if k>1
                %------------General Case for Any Dimension-----------------------
                EstN = Estimator{k}.Name;
                ZisIO2 = (isfield(Estimator{k},'Design') && strcmp(Estimator{k}.Design,'error')); % Check if IOA with error as sets
                isESE = (isfield(Estimator{k},'SetRep') && strcmp(Estimator{k}.SetRep,'ellipsoidal'));   % Check if zonotopic set representation
                %--------- Run for all reduction techniques
                if ZisIO2 || isESE
                    GE{k,1} = CumulativeTestVSE(PpD,Estimator{k},RedTech{1},ZOrder,OffGain,options,Disturbance{1});
                    Xdis = ['The completed Estimator is ', EstN, ' for plant dim ' Dim ]; disp(Xdis);
                else
                    for s = 1:length(RedTech)
                        GE{k,s} = CumulativeTestVSE(PpD,Estimator{k},RedTech{s},ZOrder,OffGain,options,Disturbance{1});
                    end
                    Xdis = ['The completed Estimator is ', EstN, ' for plant dim ', Dim, ' order', ZOrder ]; disp(Xdis);
                end
                %-------
            end
        end
        save([savepath '\' 'Vehicle_' Dim '_States_' Dist '_ZOrder_' num2str(ZOrder) '_' date '.mat'], 'Dim','Dist', 'ZOrder', 'GE')
        clear GE EstN ZOrder
    end
    clear GE Xdis Dist
    clc;
end
%------------------------------
clc;
end
%-------------------------------------------%

