function [ saturated_x ] = SaturateNoise(x,lb,ub)
%SATURATE
saturated_x = min(ub, max(lb, x));
end

