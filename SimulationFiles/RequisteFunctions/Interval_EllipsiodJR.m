function [B, I, Z] = Interval_EllipsiodJR(E)
% interval - Overapproximates an ellipsoid by an interval hull
%
% Syntax:  
%    I = interval(E)
% 
% Inputs:
%    E - ellipsoid object
%
% Outputs:
%    I - interval object
%
% Example: 
%    E = ellipsoid.generate(0);
%    I = interval(E);
%    plot(E);
%    hold on
%    plot(I);
%
% Other m-files required: interval (zonotope)
% Subfunctions: none
% MAT-files required: none
%
% See also: vertices, polytope

% Author:       Matthias Althoff
% Written:      27-June-2019 
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------
T = inv(sqrtm(E.Q));
n = E.dim;
Et = T*E;
Zt = zonotope([center(Et),eye(n)]);
Z = inv(T)*Zt;
B = box(Z);
I = interval(Z);
