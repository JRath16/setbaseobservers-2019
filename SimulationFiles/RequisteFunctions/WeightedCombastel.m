function [Zred]=  WeightedCombastel(XZ,order,W)
% W = Weighting Matrix

%initialize Z_red
Zred=XZ;

%get Z-matrix from zonotope Z
Zmatrix=generators(XZ);

%extract generator matrix
G=Zmatrix(:,2:end);

%determine dimension of zonotope
dim=length(G(:,1));


%only reduce if zonotope order is greater than the desired order
if length(G(1,:))>dim*order
    %compute metric of generators
%     h=vnorm(G,1,2);
    h = sqrt(sum(abs(G)'*W*abs(G),1));
   [~,indices]=sort(h);
    %number of generators that are not reduced
    nUnreduced=floor(dim*(order-1));
    %number of generators that are reduced
    nReduced=length(G(1,:))-nUnreduced;
    
    %pick generators that are reduced
    pickedGenerators=G(:,indices(1:nReduced));
    %compute interval hull vector d of reduced generators
    d=sum(abs(pickedGenerators),2);
    %build box Gbox from interval hull vector d
    Gbox=diag(d);
    
    %unreduced generators
    Gunred=G(:,indices((nReduced+1):end));

    %build reduced zonotope
    Zred=zonotope([center(XZ),[Gunred,Gbox]]);
    
%
end