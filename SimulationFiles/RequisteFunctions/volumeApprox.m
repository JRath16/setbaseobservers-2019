function [vol] = volumeApprox(Z)
% Syntax:  
%    [vol] = volumeApprox(Z)
%Inputs:
%    Z - zonotope object
% Outputs:
%    vol - Approximated volume

% Ref Work and Concept

% Alamo et.al, Bounded error identification of systems with time-varying
% parameters, TAC 2006
%Approximated Volume of a zonotope: A + D B^r =  2^n|det(D)|  = 2^n*sqrt(det(D*D'));   (where D is a wquare matrix);

% Example: 
%    Z=zonotope([1 -1 0; 0 0 -1]);
%    vol1=volume(Z); %The real volume
%    vol2 =  volumeApprox(Z); The approximated volume
% Author: 

G=Z.Z(:,2:end);
[dim,~]=size(G);
vol = 2^dim*(sqrt(det(G(:,1:end)*(G(:,1:end))'))); % G is the number of generators
end

