function OffGain = OffLineGainCompute(PpD)


       %---- Compute Observer gains Offline
[OffGain.SMA_PRadA,OffGain.SMA_W_PradA,OffGain.SMA_T_PradA,OffGain.ObsIndPRadA] = PRadA_BS_SMA(PpD); % PRadA- based on bisection
[OffGain.SMA_PRadB,OffGain.SMA_W_PradB,OffGain.SMA_T_PradB,OffGain.ObsIndPRadB] =  PRadB_BS_SMA(PpD); % PRadB- based on bisection---- Need to generalize  this
[OffGain.SMA_PRadC,OffGain.SMA_W_PradC,OffGain.SMA_T_PradC,OffGain.ObsIndPRadC] = PRadC_BS_SMA(PpD); % PRadC- based on bisection
[OffGain.SMA_PRadD,OffGain.SMA_W_PradD,OffGain.SMA_T_PradD,OffGain.ObsIndPradD] = PRadD_BS_SMA(PpD); % PRadD- based on bisection
[OffGain.SMA_PRadDII,OffGain.SMA_W_PradDII,OffGain.SMA_T_PradDII,OffGain.ObsIndPradDII] = PRadD_II_BS_SMA(PpD); % PRadDII- based on bisection
[OffGain.IOA_PRadA,OffGain.IOA_W_PradA,OffGain.IOA_T_PradA,OffGain.ObsIndPRadAIOA] = PRadA_BS_IOA(PpD); % PRadA- based on bisection
[OffGain.IOA_NomG,OffGain.IOA_W_NomG,OffGain.IOA_T_NomG,OffGain.ObsIndFxDcy]  = FxDecay_IOA(PpD) ; % Nominal gain
[OffGain.IOA_HInfG,OffGain.IOA_W_HInfG,OffGain.IOA_T_HInfG,OffGain.ObsIndHinfG,OffGain.Gamma_Hinf] =   HInfinity_IOA(PpD); % H inifinity
[OffGain.IOA_RobG,OffGain.IOA_W_RobG,OffGain.IOA_T_RobG,OffGain.ObsIndRobG]=  RobGain_IOA(PpD); % Robust Gain Scheduling
if size(PpD.A,1)<= 6
[OffGain.EllipIOA_PRadA,OffGain.Ellip_W_PradA,OffGain.Ellip_T_PradA,OffGain.ObsIndEllipPRadA] =  PRadA1_BS_Ellip(PpD); % ellipsoidal P-radius
[OffGain.Ellip_Hinf_1,OffGain.Ellip_W_Hinf_1,OffGain.Ellip_T_HInfG1,OffGain.Ellip_Hinf_gam_1,OffGain.Ellip_Hinf_lam_1]= HInfinity1_ESO(PpD);
[OffGain.Ellip_Hinf_2,OffGain.Ellip_W_Hinf_2,OffGain.Ellip_T_HInfG2,OffGain.Ellip_Hinf_gam_2,OffGain.Ellip_Hinf_lam_2]= HInfinity2_ESO(PpD);
end








end