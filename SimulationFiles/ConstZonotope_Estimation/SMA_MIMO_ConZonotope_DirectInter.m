function EstSet = SMA_MIMO_ConZonotope_DirectInter(Estimator,PpD,xh0,options,OffGain,Width)
% To compute the MIMO technique based state estimation USING SMA approach
ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run

ngw = size(generators(xh0.ZW),2);ngv = size(generators(xh0.ZV),2);
nx = size(PpD.A,1); ny = size(PpD.C,1);
center1  =zeros(nx,1);
width   = Width;%/.5 works partially % lower is better for bounds but have to check if Amr impelemt correct
EstSet.ZX0=conZonotope(zonotope([center1,width*eye(nx,nx)]));
A_w = zeros(1,ngw); b_w = 0;
A_v  =zeros(1,ngv); b_v = 0;
CZW = PpD.E*conZonotope([center(xh0.ZW),generators(xh0.ZW)],A_w,b_w);
CZV = conZonotope([center(xh0.ZV),generators(xh0.ZV)],A_v,b_v);
Y1 = PpD.Yp(:,1);
EstSet.ZXp(:,1) = EstSet.ZX0;EstSet.ZXh(:,1) = EstSet.ZX0;
EstSet.ZXh1(:,1) = DirectInterConZono(EstSet.ZXp(:,1),PpD,CZV,PpD.Yp(:,1));
%----------------------------------

isWtCombastel =  (isfield(options,'RedTech') && strcmp(options.RedTech,'Wtcombastel'));
if ~isWtCombastel
    %-------------------------------------------
    for k = 1:length(tspan)-2
        EstSet.ZXh(:,k) = reduce(EstSet.ZXh(:,k),options.RedTech,options.ZOrder);
        Xp_c = PpD.A*EstSet.ZXh(:,k).center + PpD.B*PpD.U(:,k);
        Xp_g = [PpD.A*EstSet.ZXh(:,k).Z(:,2:end),CZW.Z(:,2:end)];
        EstSet.ZXp(:,k+1) = conZonotope(Xp_c,Xp_g);
        EstSet.ZXh(:,k+1) = DirectInterConZono(EstSet.ZXp(:,k+1),PpD,CZV,PpD.Yp(:,k+1));
        %-- Check Emptiness
        Isemp = isempty(EstSet.ZXh(:,k+1));
        if Isemp == 1
            EstSet.ZXh(:,k+1) = EstSet.ZXp(:,k+1); %           % Refer Garcia CDC2017-- reason for outliers so doesnt intersection.
        else
            EstSet.ZXh(:,k+1) = EstSet.ZXh(:,k+1);
        end
        EstSet.ZXhp(:,k) =   EstSet.ZXh(:,k+1).center;
        EstSet.ZXpp(:,k) =   EstSet.ZXp(:,k+1).center;
    end
elseif isWtCombastel
    for k = 1:length(tspan)-2
        EstSet.ZXh(:,k) = WeightedCombastel(EstSet.ZXh(:,k),options.ZOrder,OffGain.IOA_W_NomG);
        EstSet.ZXh(:,k) = reduce(EstSet.ZXh(:,k),options.RedTech,options.ZOrder);
        Xp_c = PpD.A*EstSet.ZXh(:,k).center + PpD.B*PpD.U(:,k);
        Xp_g = [PpD.A*EstSet.ZXh(:,k).Z(:,2:end),CZW.Z(:,2:end)];
        EstSet.ZXp(:,k+1) = conZonotope(Xp_c,Xp_g);
        EstSet.ZXh(:,k+1) = DirectInterConZono(EstSet.ZXp(:,k+1),PpD,CZV,PpD.Yp(:,k+1));
        %-- Check Emptiness
        Isemp = isempty(EstSet.ZXh(:,k+1));
        if Isemp == 1
            EstSet.ZXh(:,k+1) = EstSet.ZXp(:,k+1); %           % Refer Garcia CDC2017-- reason for outliers so doesnt intersection.
        else
            EstSet.ZXh(:,k+1) = EstSet.ZXh(:,k+1);
        end
        EstSet.ZXhp(:,k) =   EstSet.ZXh(:,k+1).center;
        EstSet.ZXpp(:,k) =   EstSet.ZXp(:,k+1).center;
    end
end
end