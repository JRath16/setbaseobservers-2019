function Estimator = DirectInter()
Estimator.Name = 'DInter';
Estimator.Type = 'SMA';
Estimator.Design = 'mimo';
Estimator.SetRep = 'Conzonotopic';
Estimator.ComputeGainMIMO =@(PpD,XhH,OffGain)ComputeGainMIMO(PpD,XhH,OffGain);
end

function OGain = ComputeGainMIMO(PpD,XhH,OffGain)
Pbar = XhH*XhH';Qv = PpD.F*PpD.F';Qw = PpD.E*PpD.E';
Rbar = (PpD.A*Pbar*PpD.A' +Qw ); S = PpD.C*Rbar*PpD.C' + Qv;
L = Rbar*PpD.C'; OGain = L*inv(S);  
end