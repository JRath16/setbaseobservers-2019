%% Simulate Plant and Obserever- Set RepresenatatioBase
function [GSEstimation] = EstSimulate_ConZon(Estimator,PpD,xh0,options,OffGain,Width)
%% Initilize system
ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run

isCZSE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'Conzonotopic'));   % Check if zonotopic set representation
tic;
if isCZSE
    EstSet = SMA_MIMO_ConZonotope_DirectInter(Estimator,PpD,xh0,options,OffGain,Width);
else
    EstSet = SMA_MIMO_ConZonotope_Strips(Estimator,PpD,xh0,options,OffGain,Width);
end   

TComp = toc;
%% Performance Computation
GSEstimation.Name = Estimator.Name;GSEstimation.Type = Estimator.Type;GSEstimation.SetRep = Estimator.SetRep;
GSEstimation.ZOrder = options.ZOrder;
GSEstimation.RedTech = options.RedTech;
GSEstimation.EstStates = EstSet;
GSEstimation.TPlant = PpD;
GSEstimation.InitialObserver = xh0;
GSEstimation.SampleStepSize = options.stepSize ;
% %------------
PerfSet = PerformanceCompute_ConZon(PpD,EstSet,options);
GSEstimation.Performance = PerfSet ;
GSEstimation.CenterError = PerfSet.ERMS;
GSEstimation.IRadius = PerfSet.IRadius;
GSEstimation.Xhmax = PerfSet.Xhmax;
GSEstimation.Xhmin = PerfSet.Xhmin;
% %-------------
GSEstimation.TimePIter = TComp/length(tspan);
end