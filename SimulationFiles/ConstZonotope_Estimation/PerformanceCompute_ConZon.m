function PerfSet = PerformanceCompute_ConZon(PpD,EstSet,options)

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
for i = 2:length(tspan)-1
    %             PerfSet.BXh(:,i) = box(EstSet.ZXh(: ,i));
    PerfSet.IXh(:,i) = interval(EstSet.ZXh(: ,i));
    PerfSet.Xhmax(:,i) = supremum(PerfSet.IXh(:,i));PerfSet.Xhmin(:,i) = infimum(PerfSet.IXh(:,i));
    %             PerfSet.VolZXh(i) = volume(EstSet.ZXh(:,i));
    PerfSet.Xe(:,i) =  norm(EstSet.ZXh(:,i).center-PpD.X(:,i));
    PerfSet.IRad(:,i) = rad(PerfSet.IXh(:,i));
end
PerfSet.ERMS = rms(PerfSet.Xe);
% PerfSet.ERMS1 = rms(PerfSet.Xe);
for i = 1:size(PpD.A,1)
    PerfSet.IRadius(i) = rms(PerfSet.IRad(i,:));
end
end