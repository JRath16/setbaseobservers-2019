function [GSEstimation] = CumulativeTestVSE_ConZon(PpD,Estimator,RedTech,ZOrder,OffGain,options,Width)



Ttotal = 0:options.stepSize:options.T-options.stepSize;


%     %------ Sensor Noise%--------
%     for i = 1:size(PpD.C,1)
%         PpD.vk(:,i) = 0 + 0.33*randn(length(Ttotal),1);
%         PpD.vk(:,i) = SaturateNoise(PpD.vk(:,i),-1,1);
%     end
%  
% PpD.Yp = PpD.Y + PpD.F*PpD.vk';
%--------------
Dim = size(PpD.A,1);
options.RedTech = RedTech; % Zonotopic order reduction technique
options.ZOrder = round((ZOrder)/Dim);
    xh0.ZXin = zonotope([zeros(size(PpD.A,1),1),eye(size(PpD.A,1))]); % Initial State bounded in unity box
    xh0.ZW =  zonotope([zeros(size(PpD.E,1),1),eye(size(PpD.E,1))]); xh0.ZV = zonotope([zeros(size(PpD.C,1),1),eye(size(PpD.C,1))]);
% try
    GSEstimation = EstSimulate_ConZon(Estimator,PpD,xh0,options,OffGain,Width);
% catch
%     Xdis = ['Cannot Compute for plant dim ', Dim,  ' and reduction tech ' ,RedTech ]; disp(Xdis);
%     GSEstimation = [];
% end

end