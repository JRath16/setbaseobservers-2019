%
savepath='.\ConstZonotope_Estimation\Results_ConZon';
close all
ZnOrder= [50];%,75,100,125
options.stepSize = 0.01;
options.T = 1;%;4;
Width1 = 0.5;
ModelDim = {'6'};%,'4','6'}; % The dimensions to be tested.'4',
RedTech = {'combastel'
%             'Wtcombastel'
%             'girard'
%             'pca'    
};
Disturbance = {'random'};
%--- For the strip based method, have to tune the lambda and the initial
%bounding zonotope
Estimator = {
   DirectInter
    FRadiusMin
    PRadiusDII
    };
%-------------------------------------------%
for ja = 1:length(ModelDim)
    load(['Vehicle_' cell2mat(ModelDim(ja)) '_States_Zorder_' num2str(50) '.mat']);
    OffGain = GE{4,1}.OffLineGains;
    PpD = GE{4,1}.TPlant;    
    clear GE;
    clc;
    disp('Starting Estimation...')
    Dim = ModelDim{ja};
    Dist = Disturbance{1};
    for soo = 1:length(ZnOrder)
        ZOrder = ZnOrder(soo);
        for k = 1:length(Estimator)
            EstN1 = Estimator{k}.Name;
            for s = 1:length(RedTech)
                GE{k,s} = CumulativeTestVSE_ConZon(PpD,Estimator{k},RedTech{s},ZOrder,OffGain,options,Width1);
            end
             Xdis = ['The completed Estimator is ', EstN1, ' for plant dim ' Dim ]; disp(Xdis);
        end
        save([savepath '\' 'ConZon_' Dim '_States_' Dist '_ZOrder_' num2str(ZOrder) '_' date '.mat'], 'Dim','Dist', 'ZOrder', 'GE')
      clear GE EstN ZOrder
    end
    
  
    
end
toc;
