function EstSet = SMA_MIMO_ConZonotope_Strips2(Estimator,PpD,xh0,options,OffGain)
% To compute the MIMO technique based state estimation USING SMA approach
ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run

ngw = size(generators(xh0.ZW),2);ngv = size(generators(xh0.ZV),2);
nx = size(PpD.A,1); ny = size(PpD.C,1);
center1  =zeros(nx,1);
width   = 0.5;%/.5 works partially
EstSet.ZX0=conZonotope(zonotope([center1,width*eye(nx,nx)]));
A_w = zeros(1,ngw); b_w = 0;
A_v  =zeros(1,ngv); b_v = 0;
CZW = PpD.E*conZonotope([center(xh0.ZW),generators(xh0.ZW)],A_w,b_w);
CZV = conZonotope([center(xh0.ZV),generators(xh0.ZV)],A_v,b_v);
Y1 = PpD.Yp(:,1);
EstSet.ZXp(:,1) = EstSet.ZX0;
Y1 = PpD.Yp(:,1);
    EstSet.ZXh(:,1) = intersectConZonoStrips(EstSet.ZXp(:,1),PpD,CZV,Y1,Estimator,OffGain,1); % This estimated set
    Isemp = isempty(EstSet.ZXh(:,1));
    if Isemp == 1
        EstSet.ZXh(:,1) = EstSet.ZXp(:,1); %           % Refer Garcia CDC2017-- reason for outliers so doesnt intersection.
    else
        EstSet.ZXh(:,1) = EstSet.ZXh(:,1);
    end
%----------------------------------
isWtCombastel =  (isfield(options,'RedTech') && strcmp(options.RedTech,'Wtcombastel'));
if ~isWtCombastel
%-------------------------------------------
for k = 2:length(tspan)
    %---- Estimated Set
    Y1 = PpD.Yp(:,k);
    EstSet.ZXh(:,k) = intersectConZonoStrips(EstSet.ZXp(:,k-1),PpD,CZV,Y1,Estimator,OffGain,0); % This estimated set
    Isemp = isempty(EstSet.ZXh(:,k));
    if Isemp == 1
        EstSet.ZXh(:,k) = EstSet.ZXp(:,k-1); %           % Refer Garcia CDC2017-- reason for outliers so doesnt intersection.
    else
        EstSet.ZXh(:,k) = EstSet.ZXh(:,k);
    end
        %--- Predicted Set
    Xp_c = PpD.A*EstSet.ZXh(:,k).center + PpD.B*PpD.U(:,k);
    Xp_g = [PpD.A*EstSet.ZXh(:,k).Z(:,2:end),CZW.Z(:,2:end)];
    EstSet.ZXp(:,k) = conZonotope(Xp_c,Xp_g);
    EstSet.ZXp(:,k) = reduce(EstSet.ZXp(:,k),options.RedTech,options.ZOrder);
    EstSet.ZXhp(:,k) =   EstSet.ZXh(:,k).center;
    EstSet.ZXpp(:,k) =   EstSet.ZXp(:,k).center;
end
elseif isWtCombastel
for k = 1:length(tspan)-2
    EstSet.ZXh(:,k) = WeightedCombastel(EstSet.ZXh(:,k),options.ZOrder,OffGain.IOA_W_NomG);
    EstSet.ZU(:,k) = PpD.B*conZonotope(PpD.U(:,k),0*eye(size(PpD.U(:,k),1)));
    EstSet.ZXp(:,k+1) = PpD.A*EstSet.ZXh(:,k) + CZW + EstSet.ZU(:,k);
    Y1 = PpD.Yp(:,k+1);
    EstSet.ZXh1(:,k+1) = intersectConZonoStrips(EstSet.ZXp(:,k+1),PpD,CZV,Y1,Estimator,OffGain,1);
    %-- Check Emptiness
    Isemp = isempty(EstSet.ZXh1(:,k+1));
    if Isemp == 1
        EstSet.ZXh(:,k+1) = EstSet.ZXp(:,k+1); %           % Refer Garcia CDC2017-- reason for outliers so doesnt intersection.
    else
        EstSet.ZXh(:,k+1) = EstSet.ZXh1(:,k+1);
    end
    EstSet.ZXhp(:,k) =   EstSet.ZXh(:,k+1).center;
    EstSet.ZXpp(:,k) =   EstSet.ZXp(:,k+1).center;
% end


end


end




end