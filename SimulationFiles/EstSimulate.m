%% Simulate Plant and Obserever- Set RepresenatatioBase
function [GSEstimation] = EstSimulate(Estimator,PpD,xh0,options,OffGain)

%% Inputs:


%% Initilize system
ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%%%----Check for Design%%%%%%%%%%%
isZSE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'zonotopic'));   % Check if zonotopic set representation
isESE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'ellipsoidal'));   % Check if zonotopic set representation
ZisSMA = (isfield(Estimator,'Type') && strcmp(Estimator.Type,'SMA'));   % Check if SMA or IOA approach
ZisIOA = (isfield(Estimator,'Type') && strcmp(Estimator.Type,'IOA'));   % Check if SMA or IOA approach
ZisSIDC = (isfield(Estimator,'Design') && strcmp(Estimator.Design,'siso')); % Check if  decoupled SISO systems
ZisSIC = (isfield(Estimator,'Design') && strcmp(Estimator.Design,'mimo')); % Check if  coupled SISO systems
ZisIO1 = (isfield(Estimator,'Design') && strcmp(Estimator.Design,'state')); % Check if IOA with states as sets
ZisIO2 = (isfield(Estimator,'Design') && strcmp(Estimator.Design,'error')); % Check if SMA with states as sets
ZisPRadE = (isfield(Estimator,'Name') && strcmp(Estimator.Design,'PRadE')); % Check if PRadE technique

%%%%%%%%%%%%%%%%%%%%

%% SIMULATE OBSERVER

%------------------------------ Zonotopes---------------------------------%
if isZSE
    tic;
    %----------SISO SMA Approach
    if ZisSMA && ZisSIDC
        EstSet = SMA_SISO_Zonotope(Estimator,PpD,xh0,options,OffGain);
    end
    %-------------MIMO SMA Approach- Zonotope-------  
    
    if ZisSMA && ZisSIC &&~ZisPRadE
        EstSet = SMA_MIMO_Zonotope(Estimator,PpD,xh0,options,OffGain);
    end
    
    if ZisSMA && ZisSIC &&ZisPRadE
        EstSet = SMA_MIMO_Zonotope_PRadE(Estimator,PpD,xh0,options,OffGain);
    end
     
    %-------------IOA-States Approach- Zonotope--------
    if ZisIOA && ZisIO1
        EstSet = IOA_States_Zonotope(Estimator,PpD,xh0,options,OffGain);
    end
    %-------------IOA Error Approach- Zonotope--------
    if ZisIOA && ZisIO2
        EstSet = IOA_Error_Zonotope(Estimator,PpD,xh0,options,OffGain);
    end
    TComp = toc;
end
% %------------------     Ellipsoidal State Estimators------------------%
if isESE
    tic
    if ZisSMA && ZisSIC
        EstSet =SMA_MIMO_Ellipsoid(Estimator,PpD,xh0,options); EstSet.Intertt = 0;
    end
    if ZisIOA && ZisIO1
        EstSet =IOA_States_Ellipsoid(Estimator,PpD,xh0,options,OffGain);EstSet.Intertt = 0;
    end
    if ZisIOA && ZisIO2
        EstSet =IOA_Error_Ellipsoid(Estimator,PpD,xh0,options,OffGain);
    end
    TComp = toc-EstSet.Intertt;
end
%% Performance Computation
GSEstimation.Name = Estimator.Name;GSEstimation.Type = Estimator.Type;GSEstimation.SetRep = Estimator.SetRep;
GSEstimation.OffLineGains = OffGain;
GSEstimation.ZOrder = options.ZOrder;
GSEstimation.RedTech = options.RedTech;
GSEstimation.EstStates = EstSet;
GSEstimation.TPlant = PpD;
GSEstimation.InitialObserver = xh0;
GSEstimation.SampleStepSize = options.stepSize ;
% %--------------
PerfSet = PerformanceCompute(Estimator,PpD,EstSet,options);
GSEstimation.Performance = PerfSet ;
GSEstimation.CenterError = PerfSet.ERMS;
GSEstimation.Xhmax = PerfSet.Xhmax;
GSEstimation.Xhmin = PerfSet.Xhmin;
% if ~ZisIO2
%     GSEstimation.VolZXh = rms(PerfSet.VolZXh);
%     GSEstimation.VolBXh = rms(PerfSet.VolBXh);
% end
GSEstimation.IRadius = PerfSet.IRadius;
% %-------------
GSEstimation.TimePIter = TComp/length(tspan);

end