function [OGain,P,Tsolve,ObsInd] = PRadA_BS_Ellip(PpD)
% Paper: A New Approach for Guranteed Ellipsoidal State Estimation, IFAC
% 2014 (S. Chabbane, D. Dumur et. al);
% Solved using BMI solver
tic
ns = size(PpD.A,1); nh = size (PpD.C,1);
% neo = size(PpD.E,1); nfo = size(PpD.F,1);
% PpD.E = [PpD.E,zeros(neo,nfo)];
% PpD.F = [zeros(nfo,neo),PpD.F];
nw = size(PpD.E,2); nv = size(PpD.F,1);
try
d = nw+nv; % the total dimension of the system uncertanity vector
PpD.E = [PpD.E,zeros(nw,nv)]; % The modified E matrix
PpD.F = [zeros(nv,nw),PpD.F]; % The modified F matrix
%------Bisection Algorithm-----%
beta_up = 1;  % Max value of beta
beta_lo = 0; % Min value of beta
beta_tol = 0.01; % 0.1 
beta_wr = beta_lo; % Current working value of beta
while(beta_up-beta_lo)> beta_tol
P = sdpvar(ns,ns,'symmetric'); % The vector in state dimensions
Y = sdpvar(ns,nh,'full'); %The gain matrix
beta_tst = (beta_up+beta_lo)/2;
%% Solving the LMI Problem
GAx = blkvar;
GAx(1,1) = beta_tst*P; GAx(1,2) = 0;
GAx(1,3) = PpD.A'*P- PpD.C'*Y';
GAx(2,1) = 0;
GAx(2,2) = PpD.E'*PpD.E;
GAx(2,3)= PpD.E'*P-PpD.F'*Y';
GAx(3,1) = P*PpD.A-Y*PpD.C;
GAx(3,2) = (PpD.E-Y*PpD.F);
GAx(3,3) = P;
GAx = sdpvar(GAx);
%-----------The optimization Criterion
crit =[]; 
%% Solve the problem
options_sdp=sdpsettings;
options_sdp.solver='mosek'; % sedumi % sdpt3; lmilab
options_sdp.verbose=0;  % =0: suppress screenoutput, =1: allow screen output
% LMI problem to be solved
pblmi =  [(P>=0) , (GAx>=0)]; 
% Solve LMI conditions
solpb = optimize(pblmi,crit,options_sdp);
% Check if LMI is feasible
if solpb.problem == 1
   disp('LMIs are infeasible');
   beta_up = beta_tst;
   OGain= zeros(ns,nh);
   P = zeros(ns,ns);
else
%     disp('LMIs are FEASIBLE');
beta_lo = beta_tst;
beta_wr = beta_tst;
P = value(P);
Y = value(Y);
OGain= inv(P)*Y;
end   
beta = value(beta_wr);
end%


    PP =(PpD.A-OGain*PpD.C);
    ObsInd =  'good design'; % i.e. system has good poles
catch
    ObsInd =  'bad design';
    OGain = zeros(ns,nh);
    P = zeros(ns,ns);
end
Tsolve = toc;

end