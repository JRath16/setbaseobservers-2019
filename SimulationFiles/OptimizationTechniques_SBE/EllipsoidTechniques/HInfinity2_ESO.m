function [OGain,Pwrk,Tsolve,gamwrk,lamwrk] = HInfinity2_ESO(PpD)


% [R] Nassim Loukkas, John J. Martinez, and Nacim Meslem. Setmembership observer design based on ellipsoidal invariant
% % sets. IFAC-Papers On Line, 50(1):6471{6476, 2017


tic
F = PpD.E; Z = PpD.F; A =PpD.A; B = PpD.B;
C = PpD.C;
ns = size(PpD.A,1);nh = size(PpD.C,1);
nw = size(PpD.E,2);
nv = size(PpD.F,1);
try
a = -1; b= 1;
%------------- Start Algorithm
Q =eye(ns); 
for i = 1:2
%----------------------
gam = linspace(2,50,100);
for k = 1:length(gam)
P = sdpvar(ns,ns,'symmetric');
U = sdpvar(ns,nh,'full');
GAx = blkvar;
GAx(1,1) = -P+Q;
GAx(1,2) = 0;
GAx(1,3) = A'*P - C'*U';
GAx(2,1) = 0;
GAx(2,2) = -gam(k)*gam(k)*eye(nw+nv);
GAx(2,3) = ([P*F, - U*Z])';
GAx(3,3) = -P;
GAx = sdpvar(GAx);
ObjR= [P>=0,GAx<=0]; % The objective function   
ops_lmi = sdpsettings('solver','mosek','verbose',0,'warning',0);    
solpb = optimize(ObjR,[],ops_lmi); % optimze the LMis
if solpb.problem == 1 % see its significance in yalmiperror; checkset(pblmi)
     disp('LMIs are infeasible. Hmmm ... Be patient to wait for a miracle!')
clear P U
else
%    disp('The problem has been found FEASIBLE');
    gamma_sol  = double(gam(k));
    P = double(P);
    U = double(U);
    break
end  
end
gam = gamma_sol;
L = inv(P)*U;
%-------------------------
Ao = A-L*C;
E = [F,-L*Z];
clear Q
%------------------
Q = sdpvar(ns,ns,'symmetric');
GAx2 = blkvar;
GAx2(1,1) = Ao'*P*Ao-P+Q;
GAx2(1,2) = Ao'*P*E;
GAx2(2,1) = E'*P*A;
GAx2(2,2) = E'*P*E- gam*gam*eye(nv+nw);
GAx2 = sdpvar(GAx2);
ObjR1= [Q>=0,GAx2<=0]; % The objective function   
crit = -log(det(Q));
ops_lmi = sdpsettings('solver','penlab','verbose',0,'warning',0);    
solpb = optimize(ObjR1,crit,ops_lmi); % optimze the LMis
if solpb.problem == 1 % see its significance in yalmiperror; checkset(pblmi)
     disp('LMIs are infeasible. Hmmm ... Be patient to wait for a miracle!')
clear P U
else
%    disp('The problem has been found FEASIBLE');
   Q = double(Q);
end
%---------------------
%--------------
if i < 2
W = (1/12)*(b-a)^2*E*E';
%----
V = sdpvar(ns,ns,'symmetric');
Fa = [V>=0, Ao*V*Ao'-V<=-W];    
optimize(Fa);    
V = value(V);
Q = inv(V);
end

%---------------
%--------------------
L = inv(P)*U;
lam = min(eigs(Q,P));
Pwrk = P;
gamwrk = gamma_sol;
lamwrk = lam;

%--------------
OGain = L;
PP =(PpD.A-OGain*PpD.C);
ObsInd =  'good design'; % i.e. system has good poles
end
catch
    ObsInd =  'bad design';
    OGain = zeros(ns,nh);
    Pwrk = zeros(ns,ns);
    gamwrk = 0;
    lamwrk = 0;
    
end
Tsolve =toc;
end

%%%%%% END OF CODE %%%%%%%