function [OGain,Pwrk,Tsolve,gamwrk,lamwrk] = HInfinity1_ESO(PpD)


% % [R] John J. Martinez, Nassim Loukkas, and Nacim Meslem.
% H1 set-membership observer design for discrete-time LPV
% systems. International Journal of Control, 93(10):2314{2325,
% 2020.

% Algorithm 1 of [R]


tic
F = PpD.E; Z = PpD.F; A =PpD.A; B = PpD.B;
C = PpD.C;
ns = size(PpD.A,1);nh = size(PpD.C,1);
nw = size(PpD.E,2);
nv = size(PpD.F,1);
%------------- Start Algorithm
Q =eye(ns); 
%----------------------

gam = linspace(1,50,100);
for k = 1:length(gam)
P = sdpvar(ns,ns,'symmetric');
U = sdpvar(ns,nh,'full');
GAx = blkvar;
GAx(1,1) = -P+Q;
GAx(1,2) = 0;
GAx(1,3) = A'*P - C'*U';
GAx(2,1) = 0;
GAx(2,2) = -gam(k)*gam(k)*eye(nw+nv);
GAx(2,3) = ([P*F, - U*Z])';
GAx(3,3) = -P;
GAx = sdpvar(GAx);
ObjR= [P>=0,GAx<=0]; % The objective function   
ops_lmi = sdpsettings('solver','mosek','verbose',0,'warning',0);    
solpb = optimize(ObjR,[],ops_lmi); % optimze the LMis
if solpb.problem == 1 % see its significance in yalmiperror; checkset(pblmi)
     disp('LMIs are infeasible. Hmmm ... Be patient to wait for a miracle!')
clear P U
else
%    disp('The problem has been found FEASIBLE');
    gamma_sol  = double(gam(k));
    P = double(P);
    U = double(U);
    break
end  
end   % STEP 1 of Algo 1 of R
%----------------
gam = gamma_sol;
L = inv(P)*U; % STEP 2 of Algo 1 of R
%-------------------------
Ao = A-L*C; % STEP 3 of Algo 1 of R
E = [F,-L*Z]; % STEP 4 of Algo 1 of R
%--------------
% W = (1/12)*(b-a)^2*E*E'; 
ss = ones(1,nw+nv);
W = (1/3)*diag(ss);% STEP 5 of Algo 1 of R
%----
V = sdpvar(ns,ns,'symmetric');
Fa = [V>=0, Ao*V*Ao'-V<=-E*W*E'];    
optimize(Fa);    
V = value(V);
Q = inv(V); % STEP 6 of Algo 1 of R
%---------------
clear P U gam
%----------------------
gam = linspace(2,50,100);
for k = 1:length(gam)
P = sdpvar(ns,ns,'symmetric');
U = sdpvar(ns,nh,'full');
GAx = blkvar;
GAx(1,1) = -P+Q;
GAx(1,2) = 0;
GAx(1,3) = A'*P - C'*U';
GAx(2,1) = 0;
GAx(2,2) = -gam(k)*gam(k)*eye(nw+nv);
GAx(2,3) = ([P*F, - U*Z])';
GAx(3,3) = -P;
GAx = sdpvar(GAx);
ObjR= [P>=0,GAx<=0]; % The objective function   
ops_lmi = sdpsettings('solver','mosek','verbose',0,'warning',0);    
solpb = optimize(ObjR,gam(k),ops_lmi); % optimze the LMis
if solpb.problem == 1 % see its significance in yalmiperror; checkset(pblmi)
     disp('LMIs are infeasible. Hmmm ... Be patient to wait for a miracle!')
clear P U
else
%    disp('The problem has been found FEASIBLE');
    gamma_sol  = double(gam(k));
    P = double(P);
    U = double(U);
    break
end  
end % STEP 7 of Algo 1 of R
%--------------------
L = inv(P)*U; % STEP 8 of Algo 1 of R
lam = min(eigs(Q,P)); % STEP 9 of Algo 1 of R
Pwrk = P;
gamwrk = gamma_sol;
lamwrk = lam;
%--------------
OGain = L;
PP =(PpD.A-OGain*PpD.C);
ObsInd =  'good design'; % i.e. system has good poles




Tsolve =toc;
end

%%%%%% END OF CODE %%%%%%%