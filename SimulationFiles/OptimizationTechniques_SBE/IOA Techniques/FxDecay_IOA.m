function [OGain,W,Tsolve,ObsInd] = FxDecay_IOA(PpD)
% Description: For a LTI/LTV system there exist a bounded spv matrix(es) P
% such that the closed loop system is stable.(Refer  Oloviera 1999)
% Inputs : PpD.A, PpD.C(State, OuputMatrices)
% Ouputs: Observer Gain, matrix P, solution of lmi, decay rate
% JJRATH- 15.4.2019
tic;
ns = size(PpD.A,1);nh = size(PpD.C,1);
try
mu =1; % The decay rate
W = sdpvar(ns,ns,'symmetric'); % the spd matrix
Y = sdpvar(ns,nh,'full'); 
% Optimization
%% Solving the LMI Problem
GAx = blkvar;
GAx(1,1) = mu*W;
GAx(2,1) = W*PpD.A-Y*PpD.C;
GAx(2,2) = W;
GAx = sdpvar(GAx);
ObjR= [W>=0,GAx>=0]; % The objective function
ops_lmi = sdpsettings('solver','mosek','verbose',0,'warning',0);
sol_lmi = optimize(ObjR,[],ops_lmi); % optimze the LMis
if sol_lmi.problem==1
disp('Infeasible')
 OGain= zeros(ns,nh);
   W = zeros(ns,ns);
else
%  disp('Detectable System')
mu =value(mu);
W = value(W);
Y = value(Y);
OGain = inv(W)*Y;
end










%--- Check for obs. eigen values

PP =(PpD.A-OGain*PpD.C);
ObsInd =  'good design'; % i.e. system has good poles
    catch
    ObsInd =  'bad design';   
    OGain = zeros(ns,nh);
    W= zeros(ns,ns);
end 
    Tsolve = toc;
end

%%%%%% END OF CODE %%%%%%%