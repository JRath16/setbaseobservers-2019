function [OGain,P,Tsolve,ObsInd] = PRadA_BS_IOA(PpD)
tic;
% Use of PenLAB to solve for Beta
ns = size(PpD.A,1); nh = size (PpD.C,1);
try
%% Optimization
P = sdpvar(ns,ns,'symmetric'); % The vector in state dimensions
Th = sdpvar(ns,nh,'full'); %The gain matrix
%------Bisection Algorithm-----%
beta_up = 1;  % Max value of beta
beta_lo = 0; % Min value of beta
beta_tol = 0.01; % 0.1
beta_wr = beta_lo; % Current working value of beta
%------Start of loop
while(beta_up-beta_lo)> beta_tol
beta_tst = (beta_up+beta_lo)/2;
GAx = blkvar;
GAx(1,1) =  -beta_tst*P; GAx(1,2) = 0; GAx(1,3) = 0; GAx(1,4) = PpD.A'*P-PpD.C'*Th';
GAx(2,2) = -(PpD.E'*PpD.E); GAx(2,3) = 0; GAx(2,4) = PpD.E'*P;
GAx(3,3) = -(PpD.F'*PpD.F); 
GAx(3,4) =  PpD.F'*Th';
GAx(4,4) = -P;
GAx = sdpvar(GAx);
% Define the optimization criterion
crit =-trace(P);
%% Solve the problem
% -----------------
options_sdp=sdpsettings;
options_sdp.solver='mosek'; % sedumi 
options_sdp.shift=1e-5; % next two lines: numerical parameters
options_sdp.verbose=1;  % =0: suppress screenoutput, =1: allow screen output
% LMI problem to be solved
% LMI problem to be solved
pblmi =  [(P>=0),(GAx<=0) ]; 
% Solve LMI conditions
solpb = optimize(pblmi,crit,options_sdp);
% Check if LMI is feasible
if solpb.problem == 1
   disp('LMIs are infeasible');
   beta_up = beta_tst;
    OGain= zeros(ns,nh);
     P = zeros(ns,ns);
else
%     disp('LMIs are FEASIBLE');
beta_lo = beta_tst;
beta_wr = beta_tst;
end 
end
P= value(P);
Th = value(Th);
OGain = inv(P)*Th;


%--- Check for obs. eigen values

PP =(PpD.A-OGain*PpD.C);
ObsInd =  'good design';
    catch
    ObsInd =  'bad design';   
    OGain = zeros(ns,nh);
    P = zeros(ns,ns);
end 
Tsolve =toc;
end
%%%%%% END OF CODE %%%%%%%