function [OGain ,W,Tsolve,ObsInd] = PRadD_BS_SMA(PpD)
% RefPaper: WANGY 2018 IFAC
tic;
%-------START OF CODE-----%
ns = size(PpD.A,1); nh = size (PpD.C,1);
try
W = sdpvar(ns,ns,'symmetric');
G = eye(ns);%sdpvar(ns,ns,'diag');
O = eye(nh);%sdpvar(nh,nh,'diag');
Y = sdpvar(ns,nh,'full');
%------Bisection Algorithm-----%
beta_up = 1;  % Max value of beta
beta_lo = 0; % Min value of beta
beta_tol = 0.01; % 0.1
beta_wr = beta_lo; % Current working value of beta
%------Start of loop
while(beta_up-beta_lo)> beta_tol
    beta_tst = (beta_up+beta_lo)/2;
    GAx = blkvar;
    GAx(1,1) = beta_tst*W;
    GAx(2,2)  =G;GAx(2,1) = 0;
    GAx(3,3) = O;GAx(3,1) = 0;GAx(3,2) = 0;
    GAx(4,1) = (W-Y*PpD.C)*PpD.A;
    GAx(4,2) = (W-Y*PpD.C)*PpD.E;
    GAx(4,3) = Y*PpD.F;
    GAx(4,4) = W;
    GAx = sdpvar(GAx);
    % Define the optimization criterion
    % ---------------------------------
    crit =-trace(W);
    %% Solve the problem
    options_sdp=sdpsettings;
    options_sdp.solver='mosek'; % sedumi % sdpt3; lmilab
    options_sdp.shift=1e-5; % next two lines: numerical parameters
    options_sdp.verbose=1;  % =0: suppress screenoutput, =1: allow screen output
    % LMI problem to be solved
    pblmi = [(W>=0),(G>=0), (O>=0) ,(GAx>=0)];
    % Solve LMI conditions
    solpb = optimize(pblmi,crit,options_sdp);
    % Check if LMI is feasible
    if solpb.problem == 1
        disp('LMIs are infeasible');
        beta_up = beta_tst;
    else
        %      disp('LMIs are FEASIBLE');
        beta_lo = beta_tst;
        beta_wr = beta_tst;
    end
end
W = double(W);
Y = double(Y);
O = double(O);
G = double(G);
%-------End of While loop
OGain = inv(W)*Y;



%--- Check for obs. eigen values
    PP =(PpD.A-OGain*PpD.C);
    ObsInd =  'good design'; % i.e. system has good poles
catch
    ObsInd =  'bad design';
    OGain = zeros(ns,nh);
    W = zeros(ns,ns);
end
Tsolve = toc;
end
