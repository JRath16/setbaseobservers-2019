function [OGain ,P,gamma,Tsolve] = PRadE_BS_SMA(PpD)
% RefPaper: Ye Wang, Zhenhua Wang, Vicenc Puig, and Gabriela
%        Cembrano. Zonotopic set-membership state estimation for
%        discrete-time descriptor LPV systems. IEEE Transactions
%        on Automatic Control, 64(5):2092-2099, 2019.
tic;
%-------START OF CODE-----%
ns = size(PpD.A,1); nh = size (PpD.C,1);
nw = size(PpD.E,2); nv = size(PpD.F,1);

d = nw+nv; % the total dimension of the system uncertanity vector
E_new = [PpD.E, zeros(size(PpD.E,1),size(PpD.F,2))];
F_new = [zeros(size(PpD.F,1),size(PpD.E,2)),PpD.F];
%-------------------------------------------
% Solving Optimization Problem Eq. 33

beta = 0.5; % Choice of beta depends on designer
P = sdpvar(ns,ns,'symmetric');
Y = sdpvar(ns,nh,'full');
%---------
gamma_up = 100;  % Max value of gamma
gamma_lo = 0; % Min value of gamma
gamma_tol = 0.01; % 0.1
gamma_wr = gamma_lo; % Current working value of alpha
alpha_tst1 = 0.5;
% ------Start of loop %                   Eq. 30 in Paper
while(gamma_up-gamma_lo)> gamma_tol
gamma_tst = (gamma_up+gamma_lo)/2;
GAx = blkvar;
GAx(1,1) = alpha_tst1*P;
GAx(2,2) = (1-alpha_tst1)*beta*eye(d);
GAx(3,3) = (1-alpha_tst1)*(1-beta)*eye(d);
GAx(4,1) =  (P-Y*PpD.C)*PpD.A;%P*(eye(ns)- Y*PpD.C)*PpD.A; %
GAx(4,2) =  (P-Y*PpD.C)*E_new;%P*(eye(ns)- Y*PpD.C)*PpD.E;%
GAx(4,3) = Y*F_new;
GAx(4,4) = P;
GAx = sdpvar(GAx);
%%%%%%%%%%
%---- Eq 20
GA2x = blkvar;
GA2x(1,1) = eye(ns);
GA2x(2,1) = gamma_tst*P;
GA2x(2,2) = P;
GA2x = sdpvar(GA2x);
%-----------------
LMIs = [ (P>=0),(GAx>=0),(GA2x<=0)];
options_sdp=sdpsettings;
options_sdp.solver='mosek'; % sedumi % sdpt3; lmilab
options_sdp.shift=1e-5; % next two lines: numerical parameters
options_sdp.verbose=1;  % =0: suppress screenoutput, =1: allow screen output

solpb = optimize(LMIs,[],options_sdp);

% solpb = solvesdp(LMIs,[],options_sdp);
if solpb.problem == 1 % see its significance in yalmiperror; checkset(pblmi)
     disp('LMIs are infeasible. Hmmm ... Be patient to wait for a miracle!')
     gamma_up = gamma_tst;
else
   disp('The problem has been found FEASIBLE');
    gamma_lo = gamma_tst;
        gamma_wr = gamma_tst;
end
end

P =double(P);
Y = double(Y); 
gamma = gamma_wr; % minimum value of gamma
OGain = inv(P)*Y;
% end
Tsolve = toc;
end
