function [OGain,P,Tsolve,ObsInd]= PRadB_BS_SMA(PpD)
tic
ns = size(PpD.A,1); nh = size (PpD.C,1);
try
%------------------------
for i =1:nh
    phi(i) = max(PpD.F(i,:));
    c{i} = PpD.C(i,:);
end
%-----------------------------
%%Step 1: For the first Output compute the first lambda1
P1 = sdpvar(ns,ns,'symmetric'); % The vector in state dimensions
Y1 = sdpvar(ns,1,'full'); %The gain matrix
tau1 = sdpvar(1,1); % The factor to be maximized for decreasing the P radius
%------Bisection Algorithm-----%
beta_up = 1;  % Max value of beta
beta_lo = 0; % Min value of beta
beta_tol = 0.01; % 0.1
beta_wr = beta_lo; % Current working value of beta
%------Start of loop
while(beta_up-beta_lo)> beta_tol
    beta_tst = (beta_up+beta_lo)/2;
    GAx1 = blkvar;
    GAx1(1,1) = beta_tst*P1;
    GAx1(1,2) = 0;GAx1(1,3) = 0;GAx1(1,4) = PpD.A'*P1-PpD.A'*PpD.C(1,:)'*Y1';
    GAx1(2,2) = PpD.E'*PpD.E; GAx1(2,3) = 0;
    GAx1(2,4)= PpD.E'*P1-PpD.E'*PpD.C(1,:)'*Y1';
    GAx1(3,3) = phi(1)*phi(1);
    GAx1(3,4) =Y1'*phi(1);
    GAx1(4,4) = P1;
    GAx1 = sdpvar(GAx1);
    cond1 = ((1-beta_tst)*P1)/(phi(1)+max(norm(PpD.E)));
    % Define the optimization criterion
    % ---------------------------------
    crit =-tau1; % Maximise tau
    %% Solve the problem
    % -----------------
    options_sdp=sdpsettings;
    options_sdp.solver='mosek'; % sedumi % sdpt3; lmilab
    options_sdp.verbose=1;  % =0: suppress screenoutput, =1: allow screen output
    % LMI problem to be solved
    pblmi =  [(P1>=0) , (GAx1>=0) ,(cond1>=eye(ns)*tau1),(tau1>=0) ];
    % Solve LMI conditions
    solpb = optimize(pblmi,crit,options_sdp);
    % Check if LMI is feasible
    if solpb.problem == 1
        disp('LMIs are infeasible');
        beta_up = beta_tst;
    else
        %     disp('LMIs are FEASIBLE');
        beta_lo = beta_tst;
        beta_wr = beta_tst;
    end
end
P1 = value(P1);
Y1 = value(Y1);
tau1 = value(tau1);
Lambda{1} = inv(P1)*Y1;
%------------------------------------------------------------%
for j = 2:nh
    ss = 0;
    for i = 1:j
        NrmPhi = norm(phi(i));
        ss = ss + NrmPhi;
    end
    % c{j} = PpD.C(j,:);  % The jth row of the ouput matrix
    P{j} = sdpvar(ns,ns,'symmetric');
    Y{j} = sdpvar(ns,1,'full');
    %------------------------ Using pre computed values of lamda
    for i = 2:j
        Klam{j+1-i} = eye(ns)  - Lambda{j+1-i}*c{j+1-i};
    end
    %-----------------
    
    No_prds = j+2-3;
    
    if No_prds < 3
        if j == 2
            ProdKr.J{3} = Klam{1};
        elseif j==3
            ProdKr.J{3} = Klam{1}*Klam{2};
            ProdKr.J{4} =Klam{2};
        end
    end
    
    if No_prds >= 3
        %--------------- Prodcuts of elements based on row number
        for n = 3: j+1
            ProdKr.J{n} = Klam{j-1};
            for p = 2:j-(n-2)
                ProdKr.J{n} = ProdKr.J{n} *Klam{p}; % Product of all elements upto J-1
            end
            %-----------------
        end
        %--------------------
        %-------------------
    end
    B{1} = PpD.A'*(ProdKr.J{3})'*(P{j}-c{j}'*Y{j}');
    B{2} = PpD.E'*(ProdKr.J{3})'*(P{j}-c{j}'*Y{j}');
    if j>=3
        for n = 1:No_prds-1
            B{n+2} = (ProdKr.J{n+3}*phi(n)*Lambda{n})'*P{j} - (ProdKr.J{n+3}*phi(n)*Lambda{n})'*c{j}'*Y{j}';
        end
    end
    %-------------
    B{j+1} = (phi(j-1)*Lambda{j-1})'*P{j} - (phi(j-1)*Lambda{j-1})'*c{j}'*Y{j}';
    B{j+2} = phi(j-1)*Y{j}';
    %--------------------
    %------Bisection Algorithm-----%
    beta_up = 1;  % Max value of beta
    beta_lo = 0; % Min value of beta
    beta_tol = 0.0001;
    
    while(beta_up-beta_lo)> beta_tol
        tau(j) = sdpvar(1,1);
        beta_tst = (beta_up+beta_lo)/2;
        GAx = blkvar;
        GAx(1,1) = beta_tst*P{j};
        for i = 2:j+2
            GAx(1,i) = 0;
        end
        GAx(1,j+3) = B{1};
        GAx(2,2) = PpD.E'*PpD.E;
        for i = 3:j+2
            GAx(2,i) = 0;
        end
        GAx(2,j+3) = B{2};
        for n = 3:j+2
            GAx(n,n) = phi(n-2)*phi(n-2);
            GAx(n,j+3) = B{n};
        end
        GAx(j+3,j+3) = P{j};
        GAx = sdpvar(GAx);
        % ---------------------------------
        crit =-tau(j); % Maximise tau
        cond{j} = ((1-beta_tst)*P{j})/(ss +max(norm(PpD.E)));
        %% Solve the problem
        % -----------------
        options_sdp=sdpsettings;
        options_sdp.solver='mosek'; % sedumi % sdpt3; lmilab
        options_sdp.verbose=0;  % =0: suppress screenoutput, =1: allow screen output
        % LMI problem to be solved
        pblmi =  [(P{j}>=0) , (GAx>=0) ,(cond{j}>=eye(ns)*tau(j)),(tau(j)>=0) ];
        % Solve LMI conditions
        solpb = optimize(pblmi,crit,options_sdp);
        % Check if LMI is feasible
        if solpb.problem == 1
            disp('LMIs are infeasible');
            beta_up = beta_tst;
        else
            %     disp('LMIs are FEASIBLE');
            beta_lo = beta_tst;
            beta_wr = beta_tst;
            clear tau
        end
    end
    P{j} = value(P{j});
    Y{j} = value(Y{j});
    Lambda{j} = inv(P{j})*Y{j};
end
% clearvars -except Lambda ns nh PpD j phi c OffGain

if nh >1
    for i =1:nh
        OGain(:,i)  = Lambda{i};
    end
    P = P{j};
end

if nh<=1
    OGain = Lambda{1};
    P = P1;
end

%--- Check for obs. eigen values
PP =(PpD.A-OGain*PpD.C);
 ObsInd =  'good design'; % i.e. system has good poles
catch
    ObsInd =  'bad design';
    OGain = zeros(ns,nh);
    P = zeros(ns,ns);
end
Tsolve = toc;
end







