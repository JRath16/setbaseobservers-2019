function EstSet = IOA_States_Ellipsoid(EstSetimator,PpD,xh0,options,OffGain)

% To compute the MIMO technique based state estimation USING SMA approach

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
EstSet.EXh = xh0.EXin; EstSet.EXp = xh0.EXin; EstSet.Xhp(:,1) = center(xh0.EXin);EstSet.Xpp(:,1) = center(xh0.EXin);
EstSet.XhH{1} =eye(size(PpD.E,1));EstSet.XpH{1} =eye(size(PpD.E,1)); 


%-------------------------------------------------
for k = 1:length(tspan)-1     %  The interval observer approach with state propgation as sets
EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k) ;% Center of predicted ellipsoid at k+1 instant    
EstSet.Err(:,k) =  PpD.X(:,k)-EstSet.Xhp(:,k);    
EstSet.OGain{k} =  EstSetimator.ComputeGainMIMO(PpD,EstSet.Err(:,k),OffGain);%Design for Fradius techniques only
EstSet.Xhp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k) + EstSet.OGain{k}*(PpD.Yp(:,k)-PpD.C*EstSet.Xhp(:,k));
EstSet.EXh(:,k+1) = ellipsoid(OffGain.Ellip_W_PradA,EstSet.Xhp(:,k+1));
end
%----------------------------------------------------------

end