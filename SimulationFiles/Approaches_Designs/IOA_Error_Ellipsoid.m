function EstSet = IOA_Error_Ellipsoid(Estimator,PpD,xh0,options,OffGain)

% To compute the MIMO technique based state estimation USING SMA approach
ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
EstSet.EXh = xh0.EXin; EstSet.EXp = xh0.EXin; EstSet.Xhp(:,1) = center(xh0.EXin);EstSet.Xpp(:,1) = center(xh0.EXin);
EstSet.XhH{1} =eye(size(PpD.E,1));EstSet.XpH{1} =eye(size(PpD.E,1));

isHinfgain1  =  (isfield(Estimator,'Name') && strcmp(Estimator.Name,'HinfG_ESO1'));
isHinfgain2  =  (isfield(Estimator,'Name') && strcmp(Estimator.Name,'HinfG_ESO2'));
wup = ones(size(PpD.A,1),1)';
c1bara= ((1/OffGain.Ellip_Hinf_lam_1)*OffGain.Ellip_Hinf_gam_1*OffGain.Ellip_Hinf_gam_1*wup'*wup);
c2bara = ((1/OffGain.Ellip_Hinf_lam_2)*OffGain.Ellip_Hinf_gam_2*OffGain.Ellip_Hinf_gam_2*wup'*wup);
c1bar = c1bara(1,1);c2bar = c2bara(1,1);
e1_inf_up = diag(power(((OffGain.Ellip_W_Hinf_1)/c1bar),-0.5));
e2_inf_up = diag(power(((OffGain.Ellip_W_Hinf_2)/c2bar),-0.5));
%------- Optimization for Muo_1
tic
if isHinfgain1
    Z = zonotope([zeros(size(PpD.A,1),1),eye(size(PpD.A,1))]); % The initial state polytope/zonotope
    V= vertices(Z);
    nvert = size(V.V,2);
    Vall = V.V;
    for jj = 1:nvert
        mu = sdpvar(1,1);
        Fa = [mu>=0, (Vall(:,jj))'*OffGain.Ellip_W_Hinf_1* (Vall(:,jj))<=mu*c1bara];
        optimize(Fa);
        mu0(:,jj) = value(mu);
    end
    mu0_1 = min(mu0);
    EstSet.mu{1}  = mu0_1;
elseif isHinfgain2
    mu0_2 = max(eig(OffGain.Ellip_W_Hinf_2))*OffGain.Ellip_Hinf_gam_2*OffGain.Ellip_Hinf_gam_2/c2bar;
    EstSet.mu{1}  = mu0_2;
end
EstSet.Intertt= toc;
%-------------------------------------------------
for k = 1:length(tspan)-1     %  The interval observer approach with state propgation as sets
    EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k) ;% Center of predicted ellipsoid at k+1 instant
    EstSet.Err(:,k) =  PpD.X(:,k)-EstSet.Xhp(:,k);
    EstSet.OGain{k} =  Estimator.ComputeGainMIMO(PpD,EstSet.Err(:,k),OffGain);%Design for Fradius techniques only
    EstSet.Xhp(:,k+1) = (PpD.A -EstSet.OGain{k}*PpD.C)*EstSet.Xhp(:,k)+ PpD.B*PpD.U(:,k) + EstSet.OGain{k}*PpD.Yp(:,k);
    %----------------------
    if isHinfgain1
        EstSet.mu{k+1}  = (1-OffGain.Ellip_Hinf_lam_1)*EstSet.mu{k} + OffGain.Ellip_Hinf_lam_1;
        EstSet.Xhmax(:,k) = EstSet.Xhp(:,k+1) + e1_inf_up*power(EstSet.mu{k+1},0.5);
        EstSet.Xhmin(:,k) = EstSet.Xhp(:,k+1) - e1_inf_up*power(EstSet.mu{k+1},0.5);
    elseif isHinfgain2
        EstSet.mu{k+1}  = (1-OffGain.Ellip_Hinf_lam_2)*EstSet.mu{k} + OffGain.Ellip_Hinf_lam_2;
        EstSet.Xhmax(:,k) = EstSet.Xhp(:,k+1) + e2_inf_up*power(EstSet.mu{k+1},0.5);
        EstSet.Xhmin(:,k) = EstSet.Xhp(:,k+1) - e2_inf_up*power(EstSet.mu{k+1},0.5);
    end
    %----------------
end
%----------------------------------------------------------

end