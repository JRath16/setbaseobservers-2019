function EstSet = IOA_Error_Zonotope(Estimator,PpD,xh0,options,OffGain)

% To compute the MIMO technique based state estimation USING SMA approach

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
EstSet.ZXh = xh0.ZXin; EstSet.ZXp = xh0.ZXin; EstSet.Xhp(:,1) = center(xh0.ZXin);EstSet.Xpp(:,1) = center(xh0.ZXin);
EstSet.XhH{1} = generators(xh0.ZXin);EstSet.XpH{1} = generators(xh0.ZXin);  

isHinfgain  =  (isfield(Estimator,'Name') && strcmp(Estimator.Name,'HinfG'));
isRobGain  =  (isfield(Estimator,'Name') && strcmp(Estimator.Name,'RobG'));


if isHinfgain
 EstSet.ZS(:,1) = xh0.ZXin; EstSet.ZSw(:,1) = zonotope([zeros(size(PpD.A,1),1),0.*eye(size(PpD.A,1))]);
 EstSet.ZSv(:,1) = EstSet.ZSw(:,1);EstSet.Dw{1} = PpD.E*xh0.ZW;
 HinfGain=  Estimator.ComputeGainMIMO(PpD,EstSet.XhH{1},OffGain);
 EstSet.Dv{1} = -HinfGain*PpD.F*xh0.ZV; 
elseif isRobGain 
 EstSet.ZS(:,1) = xh0.ZXin; EstSet.ZSw(:,1) = zonotope([zeros(size(PpD.A,1),1),0.*eye(size(PpD.A,1))]);
 EstSet.ZSv(:,1) = EstSet.ZSw(:,1);EstSet.Dw{1} = PpD.E*xh0.ZW;
 RobGain= Estimator.ComputeGainMIMO(PpD,EstSet.XhH{1},OffGain);
 EstSet.Dv{1} = -RobGain*PpD.F*xh0.ZV; 
end

%-------------------------------------------------

%---FradA technique with generic reduction operators-------------%
for k = 1:length(tspan)-1     %  The interval observer approach with error propgation as sets
EstSet.Err(:,k) =  PpD.X(:,k)-EstSet.Xhp(:,k); % The estimation error
EstSet.ZErr(:,k) = box(EstSet.ZS(:,k))+ EstSet.ZSw(:,k) + EstSet.ZSv(:,k);
EstSet.IErr(:,k) = interval(EstSet.ZErr(:,k));
EstSet.Xhmax(:,k) = EstSet.Xhp(:,k) + supremum(EstSet.IErr(:,k));EstSet.Xhmin(:,k) =EstSet.Xhp(:,k) + infimum(EstSet.IErr(:,k));
EstSet.OGain{k} =  Estimator.ComputeGainMIMO(PpD,EstSet.ZErr(:,k),OffGain); % Design of gain w.r.t ID    
EstSet.Xhp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k) +EstSet. OGain{:,k}*(PpD.Yp(:,k)-PpD.C*EstSet.Xhp(:,k));
EstSet.ZS(:,k+1) = (PpD.A- EstSet.OGain{:,k}*PpD.C)*EstSet.ZS(:,k);
EstSet.ZSw(:,k+1) = EstSet.ZSw(:,k) + box((EstSet.Dw{k})); EstSet.ZSv(:,k+1) = EstSet.ZSv(:,k) + box((EstSet.Dv{k}));
EstSet.Dw{k+1} = (PpD.A- EstSet.OGain{:,k}*PpD.C)*EstSet.Dw{k} ;EstSet.Dv{k+1} = (PpD.A- EstSet.OGain{:,k}*PpD.C)*EstSet.Dv{k};     
end


end