function EstSet = SMA_MIMO_Zonotope_PRadE(Estimator,PpD,xh0,options,OffGain)

% To compute the MIMO technique based state estimation USING SMA approach

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
EstSet.ZXh = xh0.ZXin;
EstSet.ZXp = xh0.ZXin; 
EstSet.Xhp(:,1) = center(xh0.ZXin);
EstSet.Xpp(:,1) = center(xh0.ZXin);
EstSet.XhH{1} = generators(xh0.ZXin);
EstSet.XpH{1} = generators(xh0.ZXin);  
% 
% xh0.ZW

E_new = [PpD.E, zeros(size(PpD.E,1),size(PpD.F,2))];
F_new = [zeros(size(PpD.F,1),size(PpD.E,2)),PpD.F];
nw = size(generators(xh0.ZW),2); nv = size(generators(xh0.ZV),2);
xh0.ZD_c = zeros(nw+nv,1);
xh0.ZD_g = eye(nw+nv);
xh0.ZD = zonotope([xh0.ZD_c,xh0.ZD_g]);




%-----------Start------%
isWtCombastel =  (isfield(options,'RedTech') && strcmp(options.RedTech,'Wtcombastel'));
if ~isWtCombastel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
 for k = 1:length(tspan)-2         % The decoupled SMA based approach
EstSet.ZXh(:,k) = reduce(zonotope([EstSet.Xhp(:,k),EstSet.XhH{k}]),options.RedTech,options.ZOrder);
EstSet.XhH{k} = generators(EstSet.ZXh(:,k));
EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted zonotope at k+1 instant
EstSet.XpH{k+1} =[PpD.A*EstSet.XhH{k}, PpD.E_new*generators(xh0.ZD)]; % Generators of predicted zonotope at time k+1 
EstSet.ZXp(:,k+1) = zonotope([EstSet.Xpp(:,k+1),EstSet.XpH{k+1}]); % The predicted zonotope
EstSet.OGain{:,k} = Estimator.ComputeGainMIMO(PpD,EstSet.XpH{k+1},OffGain);
EstSet.Xhp(:,k+1) = EstSet.Xpp(:,k+1)+ EstSet.OGain{k}*(PpD.Yp(:,k+1)-(PpD.C*EstSet.Xpp(:,k+1))); 
EstSet.XhH{k+1} = [(eye(size(PpD.A,1))- (EstSet.OGain{k}*PpD.C))*EstSet.XpH{k+1}, EstSet.OGain{k}*PpD.F_new*generators(xh0.ZD)];
EstSet.ZXh(:,k+1) = zonotope([EstSet.Xhp(:,k+1),EstSet.XhH{k+1}]);       
 end
%-----------Using Wt.Combastel Reduction Technique--------------%     
elseif isWtCombastel
for k = 1:length(tspan)-2       
EstSet.ZXh(:,k)= WeightedCombastel(zonotope([EstSet.Xhp(:,k),EstSet.XhH{k}]),options.ZOrder,OffGain.IOA_W_NomG);
EstSet.XhH{k} = generators(EstSet.ZXh(:,k));
EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted zonotope at k+1 instant
EstSet.XpH{k+1} =[PpD.A*EstSet.XhH{k}, PpD.E_new*generators(xh0.ZD)]; % Generators of predicted zonotope at time k+1 
EstSet.ZXp(:,k+1) = zonotope([EstSet.Xpp(:,k+1),EstSet.XpH{k+1}]); % The predicted zonotope
EstSet.OGain{:,k} = Estimator.ComputeGainMIMO(PpD,EstSet.XpH{k+1},OffGain);
EstSet.Xhp(:,k+1) = EstSet.Xpp(:,k+1)+ EstSet.OGain{k}*(PpD.Yp(:,k+1)-(PpD.C*EstSet.Xpp(:,k+1))); 
EstSet.XhH{k+1} = [(eye(size(PpD.A,1))- (EstSet.OGain{k}*PpD.C))*EstSet.XpH{k+1}, EstSet.OGain{k}*PpD.F_new*generators(xh0.ZD)];
EstSet.ZXh(:,k+1) = zonotope([EstSet.Xhp(:,k+1),EstSet.XhH{k+1}]);   
end

end