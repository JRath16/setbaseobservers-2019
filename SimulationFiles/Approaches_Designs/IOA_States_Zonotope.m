function EstSet = IOA_States_Zonotope(Estimator,PpD,xh0,options,OffGain)

% To compute the MIMO technique based state estimation USING SMA approach

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
EstSet.ZXh = xh0.ZXin; EstSet.ZXp = xh0.ZXin; EstSet.Xhp(:,1) = center(xh0.ZXin);EstSet.Xpp(:,1) = center(xh0.ZXin);
EstSet.XhH{1} = generators(xh0.ZXin);EstSet.XpH{1} = generators(xh0.ZXin);  
%-------------------------------------------------
isWtCombastel =  (isfield(options,'RedTech') && strcmp(options.RedTech,'Wtcombastel'));

%---FradA technique with generic reduction operators-------------%
if ~isWtCombastel % 
for k = 1:length(tspan)-1     %  The interval observer approach with state propgation as sets
EstSet.ZXh(:,k) = reduce(zonotope([EstSet.Xhp(:,k),EstSet.XhH{k}]),options.RedTech,options.ZOrder);
EstSet.XhH{k} = generators(EstSet.ZXh(:,k));EstSet.Xhp(:,k) = center(EstSet.ZXh(:,k));
EstSet.OGain{k} =  Estimator.ComputeGainMIMO(PpD,EstSet.XhH{k},OffGain);%Design for Fradius techniques only
%----------------------------------------------------------
EstSet.Xhp(:,k+1) = (PpD.A-EstSet.OGain{k}*PpD.C)*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k) + EstSet.OGain{:,k}*PpD.Yp(:,k);        
EstSet.XhH{k+1}=[(PpD.A-EstSet.OGain{k}*PpD.C)*EstSet.XhH{k}, PpD.E*generators(xh0.ZW),-EstSet.OGain{:,k}*PpD.F*generators(xh0.ZV)];        
EstSet.ZXh(:,k+1) = zonotope([EstSet.Xhp(:,k+1),EstSet.XhH{k+1}]);      
end
end
% ----------Frad technique with Wt.Combastel Reduction opertor--------
if isWtCombastel  
for k = 1:length(tspan)-1     %  The interval observer approach with state propgation as sets
EstSet.ZXh(:,k)= WeightedCombastel(zonotope([EstSet.Xhp(:,k),EstSet.XhH{k}]),options.ZOrder,OffGain.IOA_W_NomG);
EstSet.XhH{k} = generators(EstSet.ZXh(:,k));EstSet.Xhp(:,k) = center(EstSet.ZXh(:,k));
EstSet.OGain{k} =  Estimator.ComputeGainMIMO(PpD,EstSet.XhH{k},OffGain);%Design for Fradius techniques only %Design for Fradius techniques only
% ----------------------------------------------------------
EstSet.Xhp(:,k+1) = (PpD.A-EstSet.OGain{k}*PpD.C)*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k) + EstSet.OGain{:,k}*PpD.Yp(:,k);        
EstSet.XhH{k+1}=[(PpD.A-EstSet.OGain{k}*PpD.C)*EstSet.XhH{k}, PpD.E*generators(xh0.ZW),-EstSet.OGain{:,k}*PpD.F*generators(xh0.ZV)];        
EstSet.ZXh(:,k+1) = zonotope([EstSet.Xhp(:,k+1),EstSet.XhH{k+1}]);          
end    
end

end