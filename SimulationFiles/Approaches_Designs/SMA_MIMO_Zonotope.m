function EstSet = SMA_MIMO_Zonotope(Estimator,PpD,xh0,options,OffGain)

% Ref: [R1] V. T. H. Le, C. Stoica, T. Alamo, E. F. Camacho, and
%������� D. Dumur. Zonotope-based set-membership estimation for
%������� multi-output uncertain systems. In Proc. of the IEEE
%������� International Symposium on Intelligent Control (ISIC),
%������� pages 212�217, 2013.


% To compute the MIMO technique based state estimation USING SMA approach

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
EstSet.ZXh = xh0.ZXin; EstSet.ZXp = xh0.ZXin; EstSet.Xhp(:,1) = center(xh0.ZXin);EstSet.Xpp(:,1) = center(xh0.ZXin);
EstSet.XhH{1} = generators(xh0.ZXin);EstSet.XpH{1} = generators(xh0.ZXin);  




%-----------Start------%
isWtCombastel =  (isfield(options,'RedTech') && strcmp(options.RedTech,'Wtcombastel'));
if ~isWtCombastel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
 for k = 1:length(tspan)-2         % The decoupled SMA based approach
EstSet.ZXh(:,k) = reduce(zonotope([EstSet.Xhp(:,k),EstSet.XhH{k}]),options.RedTech,options.ZOrder);
EstSet.XhH{k} = generators(EstSet.ZXh(:,k));
EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted zonotope at k+1 instant
EstSet.XpH{k+1} =[PpD.A*EstSet.XhH{k}, PpD.E*generators(xh0.ZW)]; % Generators of predicted zonotope at time k+1 
EstSet.ZXp(:,k+1) = zonotope([EstSet.Xpp(:,k+1),EstSet.XpH{k+1}]); % The predicted zonotope
EstSet.OGain{:,k} = Estimator.ComputeGainMIMO(PpD,EstSet.XpH{k+1},OffGain);
EstSet.Xhp(:,k+1) = EstSet.Xpp(:,k+1) + EstSet.OGain{k}*(PpD.Yp(:,k+1)-(PpD.C*EstSet.Xpp(:,k+1)));    
EstSet.XhH{k+1} = [(eye(size(PpD.A,1))- (EstSet.OGain{k}*PpD.C))*EstSet.XpH{k+1}, EstSet.OGain{k}*PpD.F];  % Prop 1 of R[1]
EstSet.ZXh(:,k+1) = zonotope([EstSet.Xhp(:,k+1),EstSet.XhH{k+1}]);       
 end
%-----------Using Wt.Combastel Reduction Technique--------------%     
elseif isWtCombastel
for k = 1:length(tspan)-2       
EstSet.ZXh(:,k)= WeightedCombastel(zonotope([EstSet.Xhp(:,k),EstSet.XhH{k}]),options.ZOrder,OffGain.IOA_W_NomG);
EstSet.XhH{k} = generators(EstSet.ZXh(:,k));
EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted zonotope at k+1 instant
EstSet.XpH{k+1} =[PpD.A*EstSet.XhH{k}, PpD.E*generators(xh0.ZW)]; % Generators of predicted zonotope at time k+1 
EstSet.ZXp(:,k+1) = zonotope([EstSet.Xpp(:,k+1),EstSet.XpH{k+1}]); % The predicted zonotope
EstSet.OGain{:,k} = Estimator.ComputeGainMIMO(PpD,EstSet.XpH{k+1},OffGain); 
EstSet.Xhp(:,k+1) = EstSet.Xpp(:,k+1) + EstSet.OGain{k}*(PpD.Yp(:,k+1)-(PpD.C*EstSet.Xpp(:,k+1)));    
EstSet.XhH{k+1} = [(eye(size(PpD.A,1))- (EstSet.OGain{k}*PpD.C))*EstSet.XpH{k+1}, EstSet.OGain{k}*PpD.F];% Prop 1 of R[1]
EstSet.ZXh(:,k+1) = zonotope([EstSet.Xhp(:,k+1),EstSet.XhH{k+1}]);        
end

end