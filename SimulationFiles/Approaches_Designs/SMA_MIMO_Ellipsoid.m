function EstSet = SMA_MIMO_Ellipsoid(Estimator,PpD,xh0,options)

% To compute the MIMO technique based state estimation USING SMA approach

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
EstSet.EXh = xh0.EXin; EstSet.EXp = xh0.EXin; EstSet.Xhp(:,1) = center(xh0.EXin);EstSet.Xpp(:,1) = center(xh0.EXin);
EstSet.XhH{1} =eye(size(PpD.E,1));EstSet.XpH{1} =eye(size(PpD.E,1)); EstSet.ELamOp{1} = 0;
%------------------------------------------
EstSet.ESig{1} = 1;
% EstSet.p{1} = sqrt(trace(PpD.E*PpD.E'))/ ( sqrt(trace(EstSet.ESig(1)*PpD.A*EstSet.XhH{1}*PpD.A')) + sqrt(trace(PpD.E*PpD.E')) );
EstSet.alpha = 0.00009; EstSet.Egam = max(max(PpD.F'*PpD.F));EstSet.so1 = sqrt(trace(eye(size(PpD.A,1))));
%------------------------
isKalman1996 = (isfield(Estimator,'Name') && strcmp(Estimator.Name,'Gollamudi96_ESO'));
isKalman2016 = (isfield(Estimator,'Name') && strcmp(Estimator.Name,'Liu2016_ESO'));
for k = 1:length(tspan)-2
    %% Time Update
    EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted ellipsoid at k+1 instant
    EstSet.Ep{k+1} = EstSet.so1/(sqrt(trace(EstSet.ESig{k}*EstSet.ESig{k}*PpD.A*EstSet.XhH{k}*PpD.A')) + EstSet.so1 );
    EstSet.XpH{k+1} = inv(1-EstSet.Ep{k+1})*PpD.A*EstSet.XhH{k}*PpD.A' + inv(EstSet.ESig{k}*EstSet.ESig{k}*EstSet.Ep{k+1})*PpD.E;
    EstSet.ESigP{k+1} = real(sqrt(EstSet.ESig{k})); % 
    EstSet.EXp(:,k+1) = ellipsoid(inv(EstSet.ESigP{k+1}*EstSet.ESigP{k+1}*(EstSet.XpH{k+1})),EstSet.Xpp(:,k+1)); % The predicted ellipsoid
    %---------------------------------------------------
    EstSet.EDel{k+1} =  PpD.Yp(:,k+1)-PpD.C*EstSet.Xpp(:,k+1);
    EstSet.EG{k+1} = PpD.C*EstSet.XpH{k+1}*PpD.C';
    EstSet.Eg{k+1} = svds(EstSet.EG{k+1},1);  % Compute the maximum singular values of EG
    %----------- As Vk ==1 , so E beta for both are same
    if isKalman1996
        EstSet.Ebeta{k+1} = (1- EstSet.ESigP{k+1}*EstSet.ESigP{k+1})/(EstSet.EDel{k+1}'*EstSet.EDel{k+1});
    elseif isKalman2016
        EstSet.Ebeta{k+1} = (1- EstSet.ESigP{k+1}*EstSet.ESigP{k+1})/(EstSet.EDel{k+1}'*EstSet.EDel{k+1});
    end
    %-----------
    EstSet.ELamOp{k+1} = Estimator.ComputeGainESO_MIMO(EstSet.ESigP{k+1},EstSet.EDel{k+1},EstSet.Eg{k+1},EstSet.Ebeta{k+1},EstSet.Egam,EstSet.alpha);
    %------------
    %-----------
    if isKalman2016
        EstSet.EQ{k+1}  = (1/EstSet.ELamOp{k+1})*eye(size(PpD.C,1)) + (1/(1-EstSet.ELamOp{k+1}))*PpD.C*EstSet.XpH{k+1}*PpD.C';
        EstSet.EK{k+1} = (1/(1-EstSet.ELamOp{k+1}))*EstSet.XpH{k+1}*PpD.C'*inv(EstSet.EQ{k+1});
        EstSet.ESig{k+1} = real((1-EstSet.ELamOp{k+1})*EstSet.ESigP{k+1}*EstSet.ESigP{k+1} + EstSet.ELamOp{k+1} - EstSet.EDel{k+1}'*inv(EstSet.EQ{k+1})*EstSet.EDel{k+1});
        EstSet.Xhp(:,k+1) =EstSet.Xpp(:,k+1) +EstSet.EK{k+1}*EstSet.EDel{k+1};
        EstSet.XhH{k+1} = (1/(1-EstSet.ELamOp{k+1}))*(eye(size(PpD.A,1))-EstSet.EK{k+1}*PpD.C)*EstSet.XpH{k+1};
        EstSet.EXh(:,k+1) = ellipsoid(inv(EstSet.ESig{k+1}*(EstSet.XhH{k+1})),EstSet.Xhp(:,k+1));
    elseif isKalman1996
        EstSet.EQ{k+1}  = (1-EstSet.ELamOp{k+1})*eye(size(PpD.C,1)) + EstSet.ELamOp{k+1}*EstSet.EG{k+1};
        EstSet.EP1{k+1} = (1-EstSet.ELamOp{k+1})*inv(EstSet.XpH{k+1}) + EstSet.ELamOp{k+1}*PpD.C'*PpD.C;
        EstSet.ESig{k+1} = sqrt((1-EstSet.ELamOp{k+1})*EstSet.ESigP{k+1}*EstSet.ESigP{k+1} + EstSet.ELamOp{k+1}*EstSet.Egam*EstSet.Egam -EstSet.ELamOp{k+1}*(1-EstSet.ELamOp{k+1})*EstSet.EDel{k+1}'*inv(EstSet.EQ{k+1})*EstSet.EDel{k+1} );
        EstSet.Xhp(:,k+1) = EstSet.Xpp(:,k+1) + EstSet.ELamOp{k+1}*inv(EstSet.EP1{k+1})*PpD.C'*EstSet.EDel{k+1};
        EstSet.XhH{k+1} = (EstSet.EP1{k+1});
        EstSet.EXh(:,k+1) = ellipsoid(inv (EstSet.ESig{k+1}*EstSet.ESig{k+1}*inv(EstSet.XhH{k+1})),EstSet.Xhp(:,k+1));
    end
    %-----------
    
end

end