function EstSet = SMA_MIMO_ESO(Estimator,PpD,xh0,options)

% To compute the MIMO technique based state estimation USING SMA approach

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run
%---------Initialization-----------%
% EstSet.EXh = xh0.EXin; EstSet.EXp = xh0.EXin; EstSet.Xhp(:,1) = center(xh0.EXin);EstSet.Xpp(:,1) = center(xh0.EXin);
% EstSet.Ph{1} =eye(size(PpD.E,1));EstSet.Pp{1} =eye(size(PpD.E,1)); W =eye(size(PpD.E,1)); V = eye(size(PpD.C,1));
% %------------------------------------------
% EstSet.sigH{1} = 1;
% % EstSet.p{1} = sqrt(trace(PpD.E*PpD.E'))/ ( sqrt(trace(EstSet.ESig(1)*PpD.A*EstSet.XhH{1}*PpD.A')) + sqrt(trace(PpD.E*PpD.E')) );
% EstSet.alpha = 0.9; EstSet.gamma = max(max(PpD.F'*PpD.F)); np = size(PpD.C,1);EstSet.so1 = sqrt(trace(W));
%------------------------
% isKalman1996A = (isfield(Estimator,'Name') && strcmp(Estimator.Name,'ESO_A'));
% isKalman1996B = (isfield(Estimator,'Name') && strcmp(Estimator.Name,'ESO_B'));
% isKalman2016 = (isfield(Estimator,'Name') && strcmp(Estimator.Name,'ESO_C'));

%-------------------
% if isKalman1996A || isKalman1996B
%     for k = 1:length(tspan)-2
%         EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted ellipsoid at k+1 instant
%         EstSet.sigP{k+1} = real(sqrt(EstSet.sigH{k}*EstSet.sigH{k}));
%         %------------- The predicted ellipsoid shape
%         if isKalman1996A
%             EstSet.Pp{k+1} = 2*(PpD.A*EstSet.Ph{k}*PpD.A' + (W/(EstSet.sigH{k}*EstSet.sigH{k})));
%         end
%         if isKalman1996B
%             EstSet.Pp{k+1} = PpD.A*EstSet.Ph{k}*PpD.A'  + (W/(EstSet.sigH{k}*EstSet.sigH{k})) + (2*sqrt(norm(PpD.A*EstSet.Ph{k}*PpD.A'))*sqrt(norm(W))/(EstSet.sigH{k}))*eye(size(PpD.A,1));
%         end
%         %--------------
% %         EstSet.EXp(:,k+1) =  ellipsoid((inv(EstSet.sigP{k+1}*EstSet.sigP{k+1})*inv(EstSet.Pp{k+1})),EstSet.Xpp(:,k+1)); % The predicted ellipsoid
%         %--------------------------
%         EstSet.G{k+1} = PpD.C* EstSet.Pp{k+1}*PpD.C'; EstSet.del{k+1} = PpD.Yp(:,k+1)-PpD.C*EstSet.Xpp(:,k+1);
%         EstSet.beta{k+1} = (EstSet.gamma*EstSet.gamma - EstSet.sigP{k+1}*EstSet.sigP{k+1})/(EstSet.del{k+1}'*EstSet.del{k+1} );
%         EstSet.g{k+1} = svds(EstSet.G{k+1},1);
%         %---- Obtain optimal lambda
%         EstSet.Lamda{k+1} = Estimator.ComputeGainESO_MIMO(EstSet.sigP{k+1},EstSet.del{k+1},EstSet.g{k+1},EstSet.beta{k+1},EstSet.gamma,EstSet.alpha);
% %         EstSet.Lamda{k+1} = 1;
%         %-----------------------
%         EstSet.Q{k+1} = (1- EstSet.Lamda{k+1})*eye(np) +  EstSet.Lamda{k+1}* EstSet.G{k+1};
%         EstSet.sigH2{k+1} =  (1- EstSet.Lamda{k+1})*EstSet.sigP{k+1}*EstSet.sigP{k+1} + EstSet.Lamda{k+1}*EstSet.gamma*EstSet.gamma - EstSet.Lamda{k+1}*(1-EstSet.Lamda{k+1})*EstSet.del{k+1}'*inv(EstSet.Q{k+1})*EstSet.del{k+1};
%         EstSet.sigH{k+1} = real(sqrt(EstSet.sigH2{k+1}));
%         EstSet.invPh{k+1} = (1-EstSet.Lamda{k+1})*inv(EstSet.Pp{k+1}) + EstSet.Lamda{k+1}*PpD.C'*PpD.C;
%         EstSet.Ph{k+1} = inv(EstSet.invPh{k+1});
%         EstSet.Xhp(:,k+1) = EstSet.Xpp(:,k+1) + EstSet.Lamda{k+1}*EstSet.Ph{k+1}*PpD.C'* EstSet.del{k+1};
%         EstSet.EXh(:,k+1) = ellipsoid(inv(EstSet.sigH2{k+1}*EstSet.Ph{k+1}),EstSet.Xhp(:,k+1));
%
%     end
% end
% %--------
% if isKalman2016
%     for k = 1:length(tspan)-2
%         EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted ellipsoid at k+1 instant
%         EstSet.sigP{k+1} = real(sqrt(EstSet.sigH{k}*EstSet.sigH{k}));
%         EstSet.p{k+1} = EstSet.so1/(sqrt(trace(EstSet.sigH{k}*EstSet.sigH{k}*PpD.A*EstSet.Ph{k}*PpD.A')) + EstSet.so1 );
%         EstSet.Pp{k+1} = inv(1- EstSet.p{k+1})* PpD.A*EstSet.Ph{k}*PpD.A'  + inv(EstSet.sigH{k}*EstSet.sigH{k}*EstSet.Ph{k})*W;
% %         EstSet.EXp(:,k+1) =  ellipsoid((EstSet.sigP{k+1}*EstSet.sigP{k+1}*EstSet.Pp{k+1}),EstSet.Xpp(:,k+1)); % The predicted ellipsoid
%         %--------------------
%         EstSet.del{k+1} = PpD.Yp(:,k+1)-PpD.C*EstSet.Xpp(:,k+1);EstSet.Vbar{k+1} = V;
%         EstSet.Gbar{k+1} = EstSet.Vbar{k+1}*PpD.C* EstSet.Pp{k+1}*PpD.C'*EstSet.Vbar{k+1}';
%         EstSet.delbar{k+1}=EstSet.Vbar{k+1} *EstSet.del{k+1};
%         EstSet.beta{k+1}  = (1-EstSet.sigP{k+1}*EstSet.sigP{k+1})/(EstSet.delbar{k+1}'*EstSet.delbar{k+1});
%         EstSet.gbar{k+1} = max(eig(cell2mat(EstSet.Gbar(k+1))));
% %       %---- Obtain optimal lambda
%         EstSet.Lamda{k+1} = Estimator.ComputeGainESO_MIMO(EstSet.sigP{k+1},EstSet.delbar{k+1},EstSet.gbar{k+1},EstSet.beta{k+1},EstSet.gamma,EstSet.alpha);
%         %--------------------
%         EstSet.Q{k+1} = (1/(EstSet.Lamda{k+1}))*V + (1/(1-EstSet.Lamda{k+1}))*PpD.C*EstSet.Pp{k+1}*PpD.C';
%         EstSet.Kk{k+1} =  (1/(1-EstSet.Lamda{k+1}))*EstSet.Pp{k+1}*PpD.C'*inv(EstSet.Q{k+1});
%         EstSet.sigH2{k+1} = (1-EstSet.Lamda{k+1})*EstSet.sigP{k+1}*EstSet.sigP{k+1} +  EstSet.Lamda{k+1} -EstSet.del{k+1}'*inv(EstSet.Q{k+1})*EstSet.del{k+1} ;
%         EstSet.sigH{k+1} = real(sqrt(EstSet.sigH2{k+1}));
%         EstSet.Ph{k+1} = (1/(1-EstSet.Lamda{k+1}))*(eye(size(PpD.A,1)) - EstSet.Kk{k+1}*PpD.C)* EstSet.Pp{k+1};
%         EstSet.Xhp(:,k+1)  =  EstSet.Xpp(:,k+1) + EstSet.Kk{k+1}*EstSet.del{k+1};
%         EstSet.EXh(:,k+1) = ellipsoid(inv(EstSet.sigH2{k+1}*EstSet.Ph{k+1}),EstSet.Xhp(:,k+1));
%     end
% end
%
% %---------------------

EstSet.EXh = xh0.EXin; EstSet.EXp = xh0.EXin; EstSet.Xhp(:,1) = center(xh0.EXin);EstSet.Xpp(:,1) = center(xh0.EXin);
EstSet.XhH{1} =eye(size(PpD.E,1));EstSet.XpH{1} =eye(size(PpD.E,1)); EstSet.ELamOp{1} = 0;
%------------------------------------------
EstSet.ESig{1} = 1;
% EstSet.p{1} = sqrt(trace(PpD.E*PpD.E'))/ ( sqrt(trace(EstSet.ESig(1)*PpD.A*EstSet.XhH{1}*PpD.A')) + sqrt(trace(PpD.E*PpD.E')) );
EstSet.alpha = 0.00009; EstSet.Egam = max(max(PpD.F'*PpD.F));EstSet.so1 = sqrt(trace(eye(size(PpD.A,1))));
%------------------------
isKalman1996 = (isfield(Estimator,'Name') && strcmp(Estimator.Name,'Gollamudi96_ESO'));
isKalman2016 = (isfield(Estimator,'Name') && strcmp(Estimator.Name,'Liu2016_ESO'));

for k = 1:length(tspan)-2
    EstSet.Xpp(:,k+1) = PpD.A*EstSet.Xhp(:,k) + PpD.B*PpD.U(:,k);% Center of predicted ellipsoid at k+1 instant
    EstSet.Ep{k+1} = EstSet.so1/(sqrt(trace(EstSet.ESig{k}*EstSet.ESig{k}*PpD.A*EstSet.XhH{k}*PpD.A')) + EstSet.so1 );
    EstSet.XpH{k+1} = inv(1-EstSet.Ep{k+1})*PpD.A*EstSet.XhH{k}*PpD.A' + inv(EstSet.ESig{k}*EstSet.ESig{k}*EstSet.Ep{k+1})*PpD.E;
    EstSet.ESigP{k+1} = real((EstSet.ESig{k})); %
    EstSet.EXp(:,k+1) = ellipsoid(inv(EstSet.ESigP{k+1}*EstSet.ESigP{k+1}*(EstSet.XpH{k+1})),EstSet.Xpp(:,k+1)); % The predicted ellipsoid
    if isKalman1996 
        %---------------------------------------------------
        EstSet.EDel{k+1} =  PpD.Yp(:,k+1)-PpD.C*EstSet.Xpp(:,k+1);
        EstSet.EG{k+1} = PpD.C*EstSet.XpH{k+1}*PpD.C';
        EstSet.Eg{k+1} = svds(EstSet.EG{k+1},1);  % Compute the maximum singular values of EG
        EstSet.ELamOp{k+1} = Estimator.ComputeGainESO_MIMO(EstSet.ESigP{k+1},EstSet.EDel{k+1},EstSet.Eg{k+1},EstSet.Ebeta{k+1},EstSet.Egam,EstSet.alpha);
        EstSet.EQ{k+1}  = (1-EstSet.ELamOp{k+1})*eye(size(PpD.C,1)) + EstSet.ELamOp{k+1}*EstSet.EG{k+1};
        EstSet.EP1{k+1} = (1-EstSet.ELamOp{k+1})*inv(EstSet.XpH{k+1}) + EstSet.ELamOp{k+1}*PpD.C'*PpD.C;
        EstSet.ESig{k+1} = sqrt((1-EstSet.ELamOp{k+1})*EstSet.ESigP{k+1}*EstSet.ESigP{k+1} + EstSet.ELamOp{k+1}*EstSet.Egam*EstSet.Egam -EstSet.ELamOp{k+1}*(1-EstSet.ELamOp{k+1})*EstSet.EDel{k+1}'*inv(EstSet.EQ{k+1})*EstSet.EDel{k+1} );
        EstSet.Xhp(:,k+1) = EstSet.Xpp(:,k+1) + EstSet.ELamOp{k+1}*inv(EstSet.EP1{k+1})*PpD.C'*EstSet.EDel{k+1};
        EstSet.XhH{k+1} = (EstSet.EP1{k+1});
        EstSet.EXh(:,k+1) = ellipsoid(inv (EstSet.ESig{k+1}*EstSet.ESig{k+1})*(EstSet.XhH{k+1}),EstSet.Xhp(:,k+1));
    elseif isKalman2016 
        %---------------------------------------------------
        EstSet.EDel{k+1} =  PpD.Yp(:,k+1)-PpD.C*EstSet.Xpp(:,k+1);EstSet.EDelb{k+1} =  eye(size(PpD.C,1))*EstSet.EDel{k+1} ;
        EstSet.EG{k+1} = eye(size(PpD.C,1))*PpD.C*EstSet.XpH{k+1}*PpD.C'*eye(size(PpD.C,1))';
        EstSet.Eg{k+1} = max(eig(cell2mat(EstSet.EG{k+1})));  % Compute the maximum singular values of EG
        EstSet.ELamOp{k+1} = Estimator.ComputeGainESO_MIMO(EstSet.ESigP{k+1},EstSet.EDel{k+1},EstSet.Eg{k+1},EstSet.Ebeta{k+1},EstSet.Egam,EstSet.alpha);
        EstSet.EQ{k+1}  = (1/EstSet.ELamOp{k+1})*eye(size(PpD.C,1)) + (1/(1-EstSet.ELamOp{k+1}))*PpD.C*EstSet.XpH{k+1}*PpD.C';
        EstSet.EK{k+1} = (1/(1-EstSet.ELamOp{k+1}))*EstSet.XpH{k+1}*PpD.C'*inv(EstSet.EQ{k+1});
        EstSet.ESig{k+1} = realsqrt(((1-EstSet.ELamOp{k+1})*EstSet.ESigP{k+1}*EstSet.ESigP{k+1} + EstSet.ELamOp{k+1} - EstSet.EDel{k+1}'*inv(EstSet.EQ{k+1})*EstSet.EDel{k+1}));
        EstSet.Xhp(:,k+1) =EstSet.Xpp(:,k+1) +EstSet.EK{k+1}*EstSet.EDel{k+1};
        EstSet.XhH{k+1} = (1/(1-EstSet.ELamOp{k+1}))*(eye(size(PpD.A,1))-EstSet.EK{k+1}*PpD.C)*EstSet.XpH{k+1};
        EstSet.EXh(:,k+1) = ellipsoid(inv(EstSet.ESig{k+1}*EstSet.ESig{k+1}*(EstSet.XhH{k+1})),EstSet.Xhp(:,k+1));
    end
end
end