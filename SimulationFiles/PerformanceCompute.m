function PerfSet = PerformanceCompute(Estimator,PpD,EstSet,options)

ts = options.stepSize;  % Step size of simulation
tspan = 0:ts:options.T;   % total time span for run


ZisIO2 = (isfield(Estimator,'Design') && strcmp(Estimator.Design,'error')); % Check if SMA with states as sets
isZSE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'zonotopic'));   % Check if zonotopic set representation
isESE = (isfield(Estimator,'SetRep') && strcmp(Estimator.SetRep,'ellipsoidal'));   % Check if zonotopic set representation


%---------------- Zonotopic State Estimators
if isZSE
    if ~ZisIO2
        for i = 1:length(tspan)-1
            
            PerfSet.BXh(:,i) = box(EstSet.ZXh(: ,i));
            PerfSet.IXh(:,i) = interval(EstSet.ZXh(: ,i));
            PerfSet.Xhmax(:,i) = supremum(PerfSet.IXh(:,i));PerfSet.Xhmin(:,i) = infimum(PerfSet.IXh(:,i));
            PerfSet.VolZXh(i) = volumeApprox(EstSet.ZXh(:,i));PerfSet.VolBXh(i) = volumeApprox(PerfSet.BXh(:,i));
            PerfSet.Xe(:,i) =  norm(EstSet.Xhp(:,i)-PpD.X(:,i));
            PerfSet.IRad(:,i) = rad(PerfSet.IXh(:,i));
            
        end
    elseif ZisIO2
        for i = 1:length(tspan)-1
            
            PerfSet.IRad(:,i) = 0.5*(EstSet.Xhmax(:,i)-EstSet.Xhmin(:,i));
            PerfSet.Xe(:,i) = norm(EstSet.Err(:,i));
             PerfSet.Xhmax(:,i)  = EstSet.Xhmax(:,i);PerfSet.Xhmin(:,i)  = EstSet.Xhmin(:,i);
        end
        
    end
end
%------------------ Ellipsoidal State Estimators------------------%
if isESE
    if ~ZisIO2
        for i = 1:length(tspan)-1
            [PerfSet.BXh(:,i), PerfSet.IXh(:,i),EstSet.ZXh(:,i)]  = Interval_EllipsiodJR((EstSet.EXh(i)));
            boundse = EllipsoidProj(EstSet.EXh(:,i));
            PerfSet.Xhmax(:,i) = boundse.Up;
            PerfSet.Xhmin(:,i) = boundse.Low;
            PerfSet.VolZXh(i) = volumeApprox(EstSet.ZXh(:,i));PerfSet.VolBXh(i) = volumeApprox(PerfSet.BXh(:,i));
            PerfSet.Xe(:,i) =  norm(EstSet.Xhp(:,i)-PpD.X(:,i));
            PerfSet.IRad(:,i) = 0.5*(PerfSet.Xhmax(:,i)-PerfSet.Xhmin(:,i));
        end
    end
end
if isESE && ZisIO2
    for i = 1:length(tspan)-1
        PerfSet.IRad(:,i) = 0.5*(EstSet.Xhmax(:,i)-EstSet.Xhmin(:,i));
        PerfSet.Xe(:,i) = norm(EstSet.Err(:,i));
        PerfSet.Xhmax(:,i)  = EstSet.Xhmax(:,i); PerfSet.Xhmin(:,i)  = EstSet.Xhmin(:,i);
    end
end

PerfSet.ERMS = rms(PerfSet.Xe);
% PerfSet.ERMS1 = rms(PerfSet.Xe);

for i = 1:size(PpD.A,1)
    PerfSet.IRadius(i) = rms(PerfSet.IRad(i,:));
end
end